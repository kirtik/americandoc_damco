/*
SQLyog Community v12.12 (64 bit)
MySQL - 5.6.24 : Database - americandoc_uat
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`americandoc_uat` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `americandoc_uat`;

/*Table structure for table `wp_patient_info` */

DROP TABLE IF EXISTS `wp_patient_info`;

CREATE TABLE `wp_patient_info` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `last_updated_time` timestamp(6) NULL DEFAULT NULL,
  `pname` varchar(20) DEFAULT NULL,
  `gender` enum('Male','female') DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `mother_name` varchar(20) DEFAULT NULL,
  `father_name` varchar(20) DEFAULT NULL,
  `phone_no` bigint(20) DEFAULT NULL,
  `contact_work` enum('yes','no') DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `type_of_consult` varchar(50) DEFAULT NULL,
  `current_diagnosis` varchar(100) DEFAULT NULL,
  `hope_from_consult` varchar(100) DEFAULT NULL,
  `question_by_consult` varchar(100) DEFAULT NULL,
  `name_primary_care_physician` varchar(20) DEFAULT NULL,
  `physician_address` varchar(200) DEFAULT NULL,
  `physician_phone` bigint(20) DEFAULT NULL,
  `physician_fax` bigint(50) DEFAULT NULL,
  `physician_email` varchar(20) DEFAULT NULL,
  `if_other_physician` enum('yes','no') DEFAULT NULL,
  `like_to_send_consult` enum('yes','no') DEFAULT NULL,
  `primary_medical_problem` varchar(200) DEFAULT NULL,
  `if_rheumatic_fever` enum('yes','no') DEFAULT NULL,
  `if_measles` enum('yes','no') DEFAULT NULL,
  `if_mumps` enum('yes','no') DEFAULT NULL,
  `if_heart_murmur` enum('yes','no') DEFAULT NULL,
  `if_anemia` enum('yes','no') DEFAULT NULL,
  `if_hepatitis` enum('yes','no') DEFAULT NULL,
  `if_diabetes` enum('yes','no') DEFAULT NULL,
  `if_heart_attack` enum('yes','no') DEFAULT NULL,
  `if_lung_disease` enum('yes','no') DEFAULT NULL,
  `if_kidney_disease` enum('yes','no') DEFAULT NULL,
  `if_cancer` enum('yes','no') DEFAULT NULL,
  `if_stroke` enum('yes','no') DEFAULT NULL,
  `if_tuberculosis` enum('yes','no') DEFAULT NULL,
  `if_mental_disease` enum('yes','no') DEFAULT NULL,
  `if_other_illness` enum('yes','no') DEFAULT NULL,
  `if_any_surgery` enum('yes','no') DEFAULT NULL,
  `if_any_allergy_to_medications` enum('yes','no') DEFAULT NULL,
  `current_medications` enum('yes','no') DEFAULT NULL,
  `if_stool_tested_for_blood` enum('yes','no') DEFAULT NULL,
  `if_colonoscopy` enum('yes','no') DEFAULT NULL,
  `if_blood_cholesterol_level` enum('yes','no') DEFAULT NULL,
  `if_psa` enum('yes','no') DEFAULT NULL,
  `if_chest_xray` enum('yes','no') DEFAULT NULL,
  `if_ecg` enum('yes','no') DEFAULT NULL,
  `if_mammogram` enum('yes','no') DEFAULT NULL,
  `if_pap_pelvic` enum('yes','no') DEFAULT NULL,
  `if_ctscan_abdomen` enum('yes','no') DEFAULT NULL,
  `if_liver_biopsy` enum('yes','no') DEFAULT NULL,
  `if_poor_appetite` enum('yes','no') DEFAULT NULL,
  `if_weight_loss` enum('yes','no') DEFAULT NULL,
  `if_weight_gain` enum('yes','no') DEFAULT NULL,
  `if_easy_fatigability` enum('yes','no') DEFAULT NULL,
  `if_transfusion` enum('yes','no') DEFAULT NULL,
  `if_anxiety` enum('yes','no') DEFAULT NULL,
  `if_fever` enum('yes','no') DEFAULT NULL,
  `if_itching` enum('yes','no') DEFAULT NULL,
  `if_depression` enum('yes','no') DEFAULT NULL,
  `if_eye_trouble` enum('yes','no') DEFAULT NULL,
  `if_hearing_disorder` enum('yes','no') DEFAULT NULL,
  `if_sore_tongue` enum('yes','no') DEFAULT NULL,
  `if_yellow_eyes` enum('yes','no') DEFAULT NULL,
  `if_goiter` enum('yes','no') DEFAULT NULL,
  `if_lumps` enum('yes','no') DEFAULT NULL,
  `if_chest_pain` enum('yes','no') DEFAULT NULL,
  `if_shortnessof_breath` enum('yes','no') DEFAULT NULL,
  `if_palpitations` enum('yes','no') DEFAULT NULL,
  `if_asthma` enum('yes','no') DEFAULT NULL,
  `if_chronic_cough` enum('yes','no') DEFAULT NULL,
  `if_high_bp` enum('yes','no') DEFAULT NULL,
  `if_heart_burn` enum('yes','no') DEFAULT NULL,
  `if_difficulty_swallowing` enum('yes','no') DEFAULT NULL,
  `if_indigestion` enum('yes','no') DEFAULT NULL,
  `if_milk_intolerance` enum('yes','no') DEFAULT NULL,
  `if_persistent_nausea` enum('yes','no') DEFAULT NULL,
  `if_vomiting_blood` enum('yes','no') DEFAULT NULL,
  `if_passing_blood` enum('yes','no') DEFAULT NULL,
  `if_abdominal_pain` enum('yes','no') DEFAULT NULL,
  `if_diarrhea` enum('yes','no') DEFAULT NULL,
  `if_constipation` enum('yes','no') DEFAULT NULL,
  `if_abdominal_swelling` enum('yes','no') DEFAULT NULL,
  `if_difficulty_with_urination` enum('yes','no') DEFAULT NULL,
  `if_blood_in_urine` enum('yes','no') DEFAULT NULL,
  `if_dark_urine` enum('yes','no') DEFAULT NULL,
  `if_kidney_stones` enum('yes','no') DEFAULT NULL,
  `if_difficulty_with_MP` enum('yes','no') DEFAULT NULL,
  `last_menstrual_period` enum('yes','no') DEFAULT NULL,
  `if_contraceptive_use` enum('yes','no') DEFAULT NULL,
  `if_estrogen_replacement` enum('yes','no') DEFAULT NULL,
  `if_arthritis` enum('yes','no') DEFAULT NULL,
  `if_swollen_legs` enum('yes','no') DEFAULT NULL,
  `if_cold_sensitivity` enum('yes','no') DEFAULT NULL,
  `if_recurrent_headaches` enum('yes','no') DEFAULT NULL,
  `if_loss_of_consciousness` enum('yes','no') DEFAULT NULL,
  `if_seizures` enum('yes','no') DEFAULT NULL,
  `if_loss_of_memory` enum('yes','no') DEFAULT NULL,
  `if_confusion` enum('yes','no') DEFAULT NULL,
  `if_tremor` enum('yes','no') DEFAULT NULL,
  `if_weakness` enum('yes','no') DEFAULT NULL,
  `current_weight` enum('yes','no') DEFAULT NULL,
  `lowest_weight` enum('yes','no') DEFAULT NULL,
  `highest_weight` enum('yes','no') DEFAULT NULL,
  `if_influenza` enum('yes','no') DEFAULT NULL,
  `if_tetanus` enum('yes','no') DEFAULT NULL,
  `if_pneumonia` enum('yes','no') DEFAULT NULL,
  `if_hepatitisB` enum('yes','no') DEFAULT NULL,
  `years_of_education` enum('yes','no') DEFAULT NULL,
  `occupation` enum('yes','no') DEFAULT NULL,
  `current_employment_status` enum('yes','no') DEFAULT NULL,
  `current_occupation` enum('yes','no') DEFAULT NULL,
  `if_disabled` enum('yes','no') DEFAULT NULL,
  `disabled_cause` enum('yes','no') DEFAULT NULL,
  `if_abused` enum('yes','no') DEFAULT NULL,
  `marital_status` enum('yes','no') DEFAULT NULL,
  `current_spouse` enum('yes','no') DEFAULT NULL,
  `spouse_employment_status` enum('yes','no') DEFAULT NULL,
  `spouse_current_occupation` enum('yes','no') DEFAULT NULL,
  `do_you_excercise` enum('yes','no') DEFAULT NULL,
  `if_difficulty_asleep` enum('yes','no') DEFAULT NULL,
  `if_awaken_early` enum('yes','no') DEFAULT NULL,
  `used_substances` enum('yes','no') DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `wp_patient_info` */

insert  into `wp_patient_info`(`ID`,`user_id`,`last_updated_time`,`pname`,`gender`,`dob`,`mother_name`,`father_name`,`phone_no`,`contact_work`,`email`,`address`,`type_of_consult`,`current_diagnosis`,`hope_from_consult`,`question_by_consult`,`name_primary_care_physician`,`physician_address`,`physician_phone`,`physician_fax`,`physician_email`,`if_other_physician`,`like_to_send_consult`,`primary_medical_problem`,`if_rheumatic_fever`,`if_measles`,`if_mumps`,`if_heart_murmur`,`if_anemia`,`if_hepatitis`,`if_diabetes`,`if_heart_attack`,`if_lung_disease`,`if_kidney_disease`,`if_cancer`,`if_stroke`,`if_tuberculosis`,`if_mental_disease`,`if_other_illness`,`if_any_surgery`,`if_any_allergy_to_medications`,`current_medications`,`if_stool_tested_for_blood`,`if_colonoscopy`,`if_blood_cholesterol_level`,`if_psa`,`if_chest_xray`,`if_ecg`,`if_mammogram`,`if_pap_pelvic`,`if_ctscan_abdomen`,`if_liver_biopsy`,`if_poor_appetite`,`if_weight_loss`,`if_weight_gain`,`if_easy_fatigability`,`if_transfusion`,`if_anxiety`,`if_fever`,`if_itching`,`if_depression`,`if_eye_trouble`,`if_hearing_disorder`,`if_sore_tongue`,`if_yellow_eyes`,`if_goiter`,`if_lumps`,`if_chest_pain`,`if_shortnessof_breath`,`if_palpitations`,`if_asthma`,`if_chronic_cough`,`if_high_bp`,`if_heart_burn`,`if_difficulty_swallowing`,`if_indigestion`,`if_milk_intolerance`,`if_persistent_nausea`,`if_vomiting_blood`,`if_passing_blood`,`if_abdominal_pain`,`if_diarrhea`,`if_constipation`,`if_abdominal_swelling`,`if_difficulty_with_urination`,`if_blood_in_urine`,`if_dark_urine`,`if_kidney_stones`,`if_difficulty_with_MP`,`last_menstrual_period`,`if_contraceptive_use`,`if_estrogen_replacement`,`if_arthritis`,`if_swollen_legs`,`if_cold_sensitivity`,`if_recurrent_headaches`,`if_loss_of_consciousness`,`if_seizures`,`if_loss_of_memory`,`if_confusion`,`if_tremor`,`if_weakness`,`current_weight`,`lowest_weight`,`highest_weight`,`if_influenza`,`if_tetanus`,`if_pneumonia`,`if_hepatitisB`,`years_of_education`,`occupation`,`current_employment_status`,`current_occupation`,`if_disabled`,`disabled_cause`,`if_abused`,`marital_status`,`current_spouse`,`spouse_employment_status`,`spouse_current_occupation`,`do_you_excercise`,`if_difficulty_asleep`,`if_awaken_early`,`used_substances`) values (1,1,'0000-00-00 00:00:00.000000','Poonam','female','0000-00-00','','',0,'yes','','',NULL,'','','','','',0,0,'','','yes','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','yes',''),(2,1,'0000-00-00 00:00:00.000000','Poonam','female','0000-00-00','','',0,'yes','','',NULL,'','','','new test','',0,0,'','','yes','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','yes','');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
