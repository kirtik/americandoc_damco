<style>
    .zui-table {
    border: solid 1px #DDEEEE;
    border-collapse: collapse;
    border-spacing: 0;
    font: normal 13px Arial, sans-serif;
}
.zui-table thead th {
    background-color: #9AD3E6;
    border: solid 1px #9AD3E6;
    color: #000000;
    padding: 10px;
    text-align: left;
    text-shadow: 1px 1px 1px #fff;
}
.zui-table tbody td {
    border: solid 1px #DDEEEE;
    color: #333;
    padding: 10px;
    text-shadow: 1px 1px 1px #fff;
}
.zui-table-horizontal tbody td {
    border-left: none;
    border-right: none;
}
</style>
<?php
add_action('admin_menu','user_forms');
function user_forms(){
    add_menu_page('User Forms','User Forms','administrator','user-form','user_form_list','dashicons-clipboard',6);
    add_submenu_page(null,'View User Form','View User Form','administrator','view_user_form','view_user_form_details');
}
function user_form_list(){ 
          global $wpdb;
         if(isset($_POST['deluser'])){
             $wpdb->query("DELETE FROM wp_patient_info WHERE ID='".$_POST['deluser']."'");
         }
    ?> 
<h1>Users Form Listing</h1>
<form method="post" action="">
<div style="margin-bottom:5px;">
    <?php /*?><input type="submit" name="search_user_submit" value="Search">
    <input type="text" name="search_user_form"><?php */?>
</div>
<table class="zui-table zui-table-horizontal">
    <thead>
        <tr>
            <th>S.No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th colspan="2"><center>Action<center></th>
        </tr>
    </thead>
    <tbody> 
        <?php
        $userList = get_users();
         $rowCount = 1;
         foreach($userList as $userDet){
             $userID = $userDet->ID;
             $listQuery = $wpdb->get_row("SELECT * FROM wp_patient_info WHERE user_id='".$userID."'");
             if(!empty($listQuery)){
                 $page   = 'view_user_form';
                 $formId = $listQuery->ID;
                 $record_url = add_query_arg(compact('page', 'formId'), admin_url('admin.php'));       
                 ?>
           <tr>
            <td><?php echo $rowCount;?></td>
            <td><?php echo $listQuery->pname; ?></td>
            <td><?php echo $listQuery->email; ?></td>
            <td><?php echo $listQuery->phone_no; ?></td>
            <td><a style="text-decoration: none;" href="<?php echo $record_url;?>"><button type="button" name="viewform">View Form</button></a></td>
            <td><a style="text-decoration: none;" href="#"><button type="submit" name="deluser" value="<?php echo $formId;?>" onclick="alert('You are about to delete the form');">Delete</button></a></td>      
            </tr>
                <?php
           $rowCount++;   }
        }
        ?>
    </tbody>
</table>
</form>
<?php
}

function view_user_form_details(){
    $getID = $_REQUEST['formId'];
    global $wpdb;
    $formDet = $wpdb->get_row("SELECT * FROM wp_patient_info WHERE ID='".$getID."'");
    //echo '<pre>';
    //print_r($formDet);
    //echo '</pre>';
    ?>
<h1>Form details filled by <?php echo $formDet->pname;?></h1>
<table class="zui-table zui-table-horizontal">
<tr>
    <td>Name</td>
    <td>:</td>
    <td><?php echo $formDet->pname;?></td>
</tr>
<tr>
    <td>Gender</td>
    <td>:</td>
    <td><?php echo $formDet->gender;?></td>
</tr>
<tr>
    <td>Date Of Birth</td>
    <td>:</td>
    <td><?php echo $formDet->dob;?></td>
</tr>
<tr>
    <td>Mother's Maiden Name</td>
    <td>:</td>
    <td><?php echo $formDet->mother_name;?></td>
</tr>
<tr>
    <td>Father's Full Name</td>
    <td>:</td>
    <td><?php echo $formDet->father_name;?></td>
</tr><tr>
    <td>Phone No.</td>
    <td>:</td>
    <td><?php echo $formDet->phone_no;?></td>
</tr>
<tr>
    <td>Is it ok to contact you at work?</td>
    <td>:</td>
    <td><?php echo $formDet->contact_work;?></td>
</tr>
<tr>
    <td>Email</td>
    <td>:</td>
    <td><?php echo $formDet->email;?></td>
</tr>
<tr>
    <td>Address</td>
    <td>:</td>
    <td><?php echo $formDet->address;?></td>
</tr>
<tr>
    <td>Type of Consult</td>
    <td>:</td>
    <td><?php echo $formDet->type_of_consult;?></td>
</tr>
<tr>
    <td>What is your current diagnosis/disease/disorder?</td>
    <td>:</td>
    <td><?php echo $formDet->current_diagnosis;?></td>
</tr>
<tr>
    <td>What do you hope to gain by engaging the services of an AmericanDoc Consult?</td>
    <td>:</td>
    <td><?php echo $formDet->hope_from_consult;?></td>
</tr>
<tr>
    <td>What questions do you want answered by your AmericanDoc Consult?</td>
    <td>:</td>
    <td><?php echo $formDet->question_by_consult;?></td>
</tr>
<tr>
    <td>Name of Primary Care Physician</td>
    <td>:</td>
    <td><?php echo $formDet->name_primary_care_physician?></td>
</tr>
<tr>
    <td>Physician's Address</td>
    <td>:</td>
    <td><?php echo $formDet->physician_address;?></td>
</tr>
<tr>
    <td>Physician's Phone</td>
    <td>:</td>
    <td><?php echo $formDet->physician_phone;?></td>
</tr>
<tr>
    <td>Physician's Fax</td>
    <td>:</td>
    <td><?php echo $formDet->physician_fax;?></td>
</tr>
<tr>
    <td>Physician's Email</td>
    <td>:</td>
    <td><?php echo $formDet->physician_email;?></td>
</tr>
<tr>
    <td>Other Physicians involved with your care</td>
    <td>:</td>
    <td><?php echo $formDet->if_other_physician;?></td>
</tr>
<tr>
    <td>Would you like us to send your consult to your primary care physician?</td>
    <td>:</td>
    <td><?php echo $formDet->like_to_send_consult;?></td>
</tr>
<tr>
    <td>What is the primary medical problem for which you seek evaluation information or treatment?</td>
    <td>:</td>
    <td><?php echo $formDet->primary_medical_problem;?></td>
</tr>
<tr>
    <td>Earlier had these illness</td>
    <td>:</td>
    <td>
        <?php 
            $chklist = array();
            if($formDet->if_rheumatic_fever == yes){array_push($chklist,'Rheumatic fever');}
            if($formDet->if_measles == yes){array_push($chklist,'Measles');}
            if($formDet->if_mumps == yes){array_push($chklist,'Mumps');}
            if($formDet->if_heart_mumrmur == yes){array_push($chklist,'Heart murmur');}
            if($formDet->if_anemia == yes){array_push($chklist,'Anemia');}
            if($formDet->if_hepatitis == yes){array_push($chklist,'Hepatitis');}
            if($formDet->if_diabetes == yes){array_push($chklist,'Diabetes');}
            if($formDet->if_heart_attack == yes){array_push($chklist,'Heart attack');}
            if($formDet->if_lung_disease == yes){array_push($chklist,'Lung disease');}
            if($formDet->if_kidney_disease == yes){array_push($chklist,'Kidney disease');}
            if($formDet->if_cancer == yes){array_push($chklist,'Cancer');}
            if($formDet->if_stroke == yes){array_push($chklist,'Stroke');}
            if($formDet->if_tuberculosis == yes){array_push($chklist,'Tuberculosis');}
            if($formDet->if_mental_disease == yes){array_push($chklist,'Mental Disease');}
            print_r(implode(',',$chklist));
            ?>
    </td>
</tr>
<tr>
    <td>What other illness have you had?</td>
    <td>:</td>
    <td>
        <?php $other = unserialize($formDet->if_other_illness);?>
        <table>
            <tr>
                <td>Name</td>
                <td>Approximate Date</td>
            </tr>
             <?php foreach($other as $otherill){ ?>
            <tr>
                <td><?php echo $otherill['illness'];?></td>
                <td><?php echo $otherill['illdate'];?></td>
            </tr>
            <?php    }
        ?>
        </table>        
    </td>
</tr>
<tr>
    <td>Have you had any surgery?</td>
    <td>:</td>
    <td>
    <?php $surgery = unserialize($formDet->if_any_surgery);?>
        <table>
            <tr>
                <td>Type Of Operation</td>
                <td>Approximate Date</td>
            </tr>
              <?php foreach($surgery as $surg){ ?> 
            <tr>
                <td><?php echo $surg['operation'];?></td>
                <td><?php echo $surg['opdate'];?></td>
            </tr>
            <?php    }
        ?>
        </table>
    </td>   
</tr>
<tr>
    <td>Have you had any allergy to any medications?</td>
    <td>:</td>
    <td>
      <?php $allergy = unserialize($formDet->if_any_allergy_to_medications);?>
        <table>
            <tr>
                <td>Name</td>
                <td>Type Of Operation</td>
            </tr>
                <?php foreach($allergy as $aller){ ?> 
            <tr>
                <td><?php echo $aller['allergy'];?></td>
                <td><?php echo $aller['opallergy'];?></td>
            </tr>
            <?php    }
        ?>
        </table> 
    </td>
</tr>
<tr>
    <td>Current Medications</td>
    <td>:</td>
    <td>
        <?php $medication = unserialize($formDet->current_medications);?>
        <table>
            <tr>
                <td>Name</td>
                <td>Dose</td>
                <td>Frequency</td>
            </tr>
            <?php foreach($medication as $medi){ ?> 
            <tr>
                <td><?php echo $medi['name'];?></td>
                <td><?php echo $medi['dose'];?></td>
                <td><?php echo $medi['frequency'];?></td>
            </tr>
            <?php } ?>
        </table> 
    </td>
</tr>
<tr>
    <td>In the past 3 years</td>
    <td>:</td>
    <td>
        <?php 
            $chklist2 = array();
            if($formDet->if_stool_tested_for_blood == yes){array_push($chklist2,'Stool Tested For Blood');}
            if($formDet->if_colonoscopy == yes){array_push($chklist2,'Flexible sigmoidoscopy or colonoscopy');}
            if($formDet->if_blood_cholesterol_level == yes){array_push($chklist2,'Blood cholesterol level');}
            if($formDet->if_psa == yes){array_push($chklist2,'Prostatic specific antigen test (PSA)');}
            if($formDet->if_chest_xray == yes){array_push($chklist2,'Chest x-ray');}
            if($formDet->if_ecg == yes){array_push($chklist2,' Electrocardiogram (ECG)');}
            if($formDet->if_mammogram == yes){array_push($chklist2,'Mammogram');}
            if($formDet->if_pap_pelvic == yes){array_push($chklist2,'Pap Smear and Pelvic exam');}
            if($formDet->if_ctscan_abdomen == yes){array_push($chklist2,'CT scan of abdomen');}
            if($formDet->if_liver_biopsy == yes){array_push($chklist2,'Liver biopsy');}
            print_r(implode(',',$chklist2));
            ?>
    </td>
</tr>
<tr>
    <td colspan="3"><strong>REVIEW OF SYSTEMS</strong></td>
</tr>
<tr>
    <td>Have had following problems</td>
    <td>:</td>
    <td><strong>General</strong></td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
        <?php 
            $general = array();
            if($formDet->if_poor_appetite == yes){array_push($general,'Poor Appetite');}
            if($formDet->if_weight_loss == yes){array_push($general,'Weight Loss');}
            if($formDet->if_weight_gain == yes){array_push($general,'Weight Gain');}
            if($formDet->if_easy_fatigability == yes){array_push($general,'Easy Fatigability');}
            if($formDet->if_transfusion == yes){array_push($general,'Transfusion');}
            if($formDet->if_anxiety == yes){array_push($general,'Anxiety');}
            if($formDet->if_fever == yes){array_push($general,'Fever or Abnormal Sweating');}
            if($formDet->if_itching == yes){array_push($general,'Itching');}
            if($formDet->if_depression == yes){array_push($general,'Depression');}
            print_r(implode(',',$general));
            ?>
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
        <strong>Head</strong>
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
        <?php 
            $headdetails = array();
            if($formDet->if_eye_trouble == yes){array_push($headdetails,'Eye Trouble');}
            if($formDet->if_hearing_disorder == yes){array_push($headdetails,'Hearing Disorder');}
            if($formDet->if_sore_tongue == yes){array_push($headdetails,'Sore Tongue or Mouth');}
            if($formDet->if_yellow_eyes == yes){array_push($headdetails,'Yellow Eyes');}
            print_r(implode(',',$headdetails));
            ?>
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
        <strong>Neck</strong>
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
        <?php 
            $neck = array();
            if($formDet->if_goiter == yes){array_push($neck,'Goiter');}
            if($formDet->if_lumps == yes){array_push($neck,'Lumps or Masses');}
            print_r(implode(',',$neck));
            ?>
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
        <strong>Chest</strong>
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
         <?php 
            $chest = array();
            if($formDet->if_chest_pain == yes){array_push($chest,'Chest Pain');}
            if($formDet->if_shortnessof_breath == yes){array_push($chest,'Shortness of Breath');}
            if($formDet->if_palpitations == yes){array_push($chest,'Palpitations');}
            if($formDet->if_asthma == yes){array_push($chest,'Asthma');}
            if($formDet->if_chronic_cough == yes){array_push($chest,'Chronic Cough');}
            if($formDet->if_high_bp == yes){array_push($chest,'High Blood Pressure');}            
            print_r(implode(',',$chest));
            ?>
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
        <strong>GI</strong>
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
        <?php 
            $gi = array();
            if($formDet->if_heart_burn == yes){array_push($gi,'Heart Burn');}
            if($formDet->if_difficulty_swallowing == yes){array_push($gi,'Difficulty Swallowing');}
            if($formDet->if_indigestion == yes){array_push($gi,'Indigestion');}
            if($formDet->if_milk_intolerance == yes){array_push($gi,'Milk Intolerance');}
            if($formDet->if_persistent_nausea == yes){array_push($gi,'Persistent Nausea or Vomiting');}
            if($formDet->if_vomiting_blood == yes){array_push($gi,'Vomiting Blood');}
            if($formDet->if_passing_blood == yes){array_push($gi,'Passing Blood');}
            if($formDet->if_abdominal_pain == yes){array_push($gi,'Abdominal Pain');}
            if($formDet->if_diarrhea == yes){array_push($gi,'Diarrhea');}
            if($formDet->if_constipation == yes){array_push($gi,'Constipation');}
            if($formDet->if_abdominal_swelling == yes){array_push($gi,'Abdominal Swelling');}
            print_r(implode(',',$gi));
            ?>
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
        <strong>GU</strong>
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
        <?php 
            $gu = array();
            if($formDet->if_difficulty_with_urination == yes){array_push($gu,'Difficulty With Urination');}
            if($formDet->if_blood_in_urine == yes){array_push($gu,'Blood in Urine');}
            if($formDet->if_dark_urine == yes){array_push($gu,'Dark Urine');}
            if($formDet->if_kidney_stones == yes){array_push($gu,'Kidney Stones');}
            print_r(implode(',',$gu));
            ?>
    </td>
</tr>
<?php if($formDet->gender == 'female'){ ?>
<tr>
    <td></td>
    <td></td>
    <td><strong>Difficulty with Menstrual period : <?php echo $formDet->if_difficulty_with_MP;?></strong></td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
        <?php 
            $fem = array();
            if($formDet->last_menstrual_period == yes){array_push($fem,'Last Menstrual Period');}
            if($formDet->if_contraceptive_use == yes){array_push($fem,'Contraceptive Use');}
            if($formDet->if_estrogen_replacement == yes){array_push($fem,'Estrogen Replacement');}
            print_r(implode(',',$fem));
            ?>
    </td>
</tr>
<?php } ?>
<tr>
    <td></td>
    <td></td>
    <td><strong>Extremities</strong></td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
        <?php 
            $exter = array();
            if($formDet->if_arthritis == yes){array_push($exter,'Arthritis');}
            if($formDet->if_swollen_legs == yes){array_push($exter,'Swollen Legs');}
            if($formDet->if_cold_sensitivity == yes){array_push($exter,'Cold Sensitivity');}
            print_r(implode(',',$exter));
            ?>    
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td><strong>Neurologic</strong></td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
        <?php 
            $neuro = array();
            if($formDet->if_recurrent_headaches == yes){array_push($neuro,'Recurrent Headaches');}
            if($formDet->if_loss_of_consciousness == yes){array_push($neuro,'Loss of Consciousness');}
            if($formDet->if_seizures == yes){array_push($neuro,'Seizures');}
            if($formDet->if_loss_of_memory == yes){array_push($neuro,'Loss of Memory');}
            if($formDet->if_confusion == yes){array_push($neuro,'Confusion');}
            if($formDet->if_tremor == yes){array_push($neuro,'Tremor');}
            if($formDet->if_weakness == yes){array_push($neuro,'Weakness or numbness of face or extremities');}
            print_r(implode(',',$neuro));
            ?>    
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td><strong>Current Weight :</strong> <?php echo $formDet->current_weight;?></td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td><strong>Lowest Weight :</strong> <?php echo $formDet->lowest_weight;?></td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td><strong>Highest Weight :</strong> <?php echo $formDet->highest_weight;?></td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Immunization</td>
    <td>:</td>
    <td>
        <?php 
            $immune = array();
            if($formDet->if_influenza == yes){array_push($immune,'Influenza');}
            if($formDet->if_tetanus == yes){array_push($immune,'Tetanus/Diptheria');}
            if($formDet->if_pneumonia == yes){array_push($immune,'Pneumonia');}
            if($formDet->if_hepatitisB == yes){array_push($immune,'Hepatitis B');}
            print_r(implode(',',$immune));
            ?>
    </td>
</tr>
<tr>
    <td><strong>Education</strong></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>How many years of school have you completed?</td>
    <td>:</td>
    <td><?php echo $formDet->years_of_education;?></td>
</tr>
<tr>
    <td><strong>Occupation</strong></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Current employment status</td>
    <td>:</td>
    <td><?php echo $formDet->current_employment_status;?></td>
</tr>
<?php if($formDet->current_employment_status == 'employed'){?>
<tr>
    <td>Current Occupation</td>
    <td>:</td>
    <td><?php echo $formDet->current_occupation;?></td>
</tr>
<?php } ?>
<tr>
    <td>Previous Occupation</td>
    <td>:</td>
    <td><?php echo $formDet->previous_occupation;?></td>
</tr>
<tr>
    <td><strong>Disability</strong></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Disabled</td>
    <td>:</td>
    <td><?php echo $formDet->if_disabled;?></td>
</tr>
<?php if($formDet->if_disabled == 'yes'){?>
<tr>
    <td>Cause</td>
    <td>:</td>
    <td><?php echo $formDet->disabled_cause;?></td>
</tr>
<?php } ?>
<tr>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Have you ever been physically, sexually or emotionally abused ?</td>
    <td>:</td>
    <td><?php echo $fromDet->if_abused;?></td>
</tr>
<tr>
    <td>Use of substances</td>
    <td>:</td>
    <td>
        <table>
        <?php $subst = unserialize($formDet->used_substances);?>
            <tr>
            <td>Substance</td>
            <td>Current Use</td>
            <td>Previous Use</td>
            <td>Type/Amount</td>
            <td>Length/If stopped when</td>
        </tr>
        <tr>
            <td>Caffeine (coffee, tea, soda)</td>
            <td><?php echo $subst['caffcurrent']?></td>    
            <td><?php echo $subst['caffprevious']?></td>
            <td><?php echo $subst['cafftype']?></td>
            <td><?php echo $subst['cafflength']?></td>
        </tr> 
        <tr>
            <td>Tobacco</td>
            <td><?php echo $subst['tobcurrent']?></td>    
            <td><?php echo $subst['tobprevious']?></td>
            <td><?php echo $subst['tobtype']?></td>
            <td><?php echo $subst['toblength']?></td>
        </tr> 
        <tr>
            <td>Alcohol (beer, wine, liquor)</td>
            <td><?php echo $subst['alcurrent']?></td>    
            <td><?php echo $subst['alprevious']?></td>
            <td><?php echo $subst['altype']?></td>
            <td><?php echo $subst['allength']?></td>
        </tr> 
        <tr>
            <td>Recreational/street drugs</td>
            <td><?php echo $subst['drcurrent']?></td>    
            <td><?php echo $subst['drprevious']?></td>
            <td><?php echo $subst['drtype']?></td>
            <td><?php echo $subst['drlength']?></td>
        </tr> 
            </table>
    </td>
</tr>
<tr>
    <td>Marital Status</td>
    <td>:</td>
    <td><?php echo $formDet->marital_status;?></td>
</tr>
<tr>
    <td>Current Spouse</td>
    <td>:</td>
    <td><?php echo $formDet->current_spouse?></td>
</tr>
<tr>
    <td>Current employment status</td>
    <td>:</td>
    <td><?php echo $formDet->spouse_employment_status;?></td>
</tr>
<?php if($formDet->spouse_employment_status == 'employed'){?>
<tr>
    <td>Current occupation </td>
    <td>:</td>
    <td><?php echo $formDet->spouse_current_occupation;?></td>
</tr>
<?php } ?>
<tr>
    <td>Do you exercise regularly?</td>
    <td>:</td>
    <td><?php echo $formDet->do_you_excercise;?></td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td><strong>Sleep</strong></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Do you have difficulty falling asleep?</td>
    <td>:</td>
    <td><?php echo $formDet->if_difficulty_asleep;?></td>
</tr>
<tr>
    <td>Do you awaken early in the morning without apparent cause?</td>
    <td>:</td>
    <td><?php echo $formDet->if_awaken_early;?></td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Family History If living If Deceased</td>
    <td>:</td>
    <td>
        <table>
        <?php $family = unserialize($formDet->family_history);?>
        <tr>
            <td>Relation</td>
            <td>Age</td>
            <td>Health</td>
            <td>Age of Death</td>
            <td>Cause</td>
        </tr>
        <tr>
            <td>Father</td>
            <td><?php echo $family['fage'];?></td>
            <td><?php echo $family['fhealth'];?></td>
            <td><?php echo $family['faged'];?></td>
            <td><?php echo $family['fcause'];?></td>
        </tr>
        <tr>
            <td>Mother</td>
            <td><?php echo $family['mage'];?></td>
            <td><?php echo $family['mhealth'];?></td>
            <td><?php echo $family['maged'];?></td>
            <td><?php echo $family['mcause'];?></td>
        </tr>
        <tr>
            <td><?php echo $family['sibling'];?></td>
            <td><?php echo $family['sage'];?></td>
            <td><?php echo $family['shealth'];?></td>
            <td><?php echo $family['saged'];?></td>
            <td><?php echo $family['scause'];?></td>
        </tr>
        <tr>
            <td><?php echo $family['husband'];?></td>
            <td><?php echo $family['hage'];?></td>
            <td><?php echo $family['hhealth'];?></td>
            <td><?php echo $family['haged'];?></td>
            <td><?php echo $family['hcause'];?></td>
        </tr>
        <tr>
            <td><?php echo $family['son'];?></td>
            <td><?php echo $family['soage'];?></td>
            <td><?php echo $family['sohealth'];?></td>
            <td><?php echo $family['soaged'];?></td>
            <td><?php echo $family['socause'];?></td>
        </tr>
            </table>
    </td>
</tr>
<tr>
    <td>Blood Relatives</td>
    <td>:</td>
    <td>
        <table>
        <?php 
        $bloodrel = unserialize($formDet->blood_relative);
        foreach($bloodrel as $key=>$value){
            if(!empty($value)){
            echo '<tr><td>'.$key.'</td><td>'.$value.'</td></tr>';
            }
        }
        ?>
        </table>    
    </td>
</tr>
<tr>
    <td><strong>Uploaded Documents</strong></td>
    <td>:</td>
    <td>
        <?php
        $upfile = $formDet->user_id;
        global $wpdb;
        $uploadDocument = $wpdb->get_results("SELECT * FROM `wp_wfu_log` WHERE userid='".$upfile."'");
        foreach($uploadDocument as $updoc){ 
            $filename = basename($updoc->filepath);
            $filepath = $updoc->filepath;
            ?>
            <a href="<?php echo $filepath;?>" download="<?php echo $filename;?>"><?php echo $filename;?></a><br>
        <?php }
    ?>
    </td>
</tr>
</table>
<?php
}
