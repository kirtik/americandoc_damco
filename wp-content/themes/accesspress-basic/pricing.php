<?php
// Template Name: Pricing Template
?>
<?php
if(isset($_POST['price_submit'])){
    $user = get_current_user_id();
    global $wpdb;
    $userForm = $wpdb->get_row("SELECT * FROM wp_patient_info WHERE user_id='".$user."'");
    $wpdb->query("INSERT INTO wp_pricing (user_id,form_id,single_speciality,multi_speciality,orthopedic_cardiac,diabetes_consult,mirrored_care_service,nutrition_consult,speedy_service,Total)VALUES('".$user."','".$userForm->ID."','".$_POST['single_specialty']."','".$_POST['multi_specialty']."','".$_POST['orthopedic_cardiac']."','".$_POST['diabetes_consult']."','".$_POST['Mirrored_care_service']."','".$_POST['nutrition_consult']."','".$_POST['speedy_service']."','".$_POST['total']."')");
    $rdUrl = site_url().'/paymentpage/';
    header('Location:'.$rdUrl);    
}
global $wpdb;
$chkUser = get_current_user_id();
$rowCheck = $wpdb->get_row("SELECT * FROM wp_payment_details WHERE user_id='".$chkUser."'");
if($rowCheck){
    ?>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    $('#primary').hide();
    $('#pricing-message').show();
    });
    </script>
<?php
}
else 
{ ?>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
    $('#primary').show();
    $('#pricing-message').hide();
    });
    </script>
<?php }    
global $apbasic_options;
$apbasic_settings = get_option('apbasic_options', $apbasic_options);
if ( is_array( $apbasic_settings ) && ! empty( $apbasic_settings )) {
    extract($apbasic_settings);
}
get_header(); 


?>

<?php 
    $single_default_page_layout = get_post_meta( $post->ID, 'apbasic_page_layout', true);
    $default_page_layout = ($single_default_page_layout == 'default_layout') ? $default_page_layout : $single_default_page_layout;
    
    // Dynamically Generating Classes for #primary on the basis of page layout
    $content_class = '';
    switch($default_page_layout){
        case 'left_sidebar':
            $content_class = 'left-sidebar';
            break;
        case 'right_sidebar':
            $content_class = 'right-sidebar';
            break;
        case 'both_sidebar':
            $content_class = 'both-sidebar';
            break;
        case 'no_sidebar_wide':
            $content_class = 'no-sidebar-wide';
            break;
        case 'no_sidebar_narrow':
            $content_class = 'no-sidebar-narraow';
            break;
    }
?>
<?php while ( have_posts() ) : the_post(); ?>
	<main id="main" class="site-main <?php echo $content_class; ?>" role="main">
        <div class="ap-container">
        <?php if($default_page_layout == 'both_sidebar') : ?>
            <div id="primary-wrap" class="clearfix">
        <?php endif; ?> 
        
            <div id="primary" class="content-area">
					
<form id="pricing" action="" method="post">
<table>
<tbody>
<tr>
<th></th>
<th>Type of Consult</th>
<th>Pricing</th>
</tr>
<tr>
<td><input class="pricing" name="single_specialty" type="checkbox" value="10000" /></td>
<td>Single Specialty Consults</td>
<td>10000 Rs</td>
</tr>
<tr>
<td><input class="pricing" name="multi_specialty" type="checkbox" value="15000" /></td>
<td>Multi Specialty Consults</td>
<td>15000 Rs</td>
</tr>
<tr>
<td><input class="pricing" name="orthopedic_cardiac" type="checkbox" value="30000" /></td>
<td>Orthopedic/Cardiac Surgeon Consult</td>
<td>30000 Rs</td>
</tr>
<tr>
<td><input class="pricing" name="diabetes_consult" type="checkbox" value="12500" /></td>
<td>Diabetes Consult</td>
<td>12500 Rs</td>
</tr>
<tr>
<td><input class="pricing" name="Mirrored_care_service" type="checkbox" value="40000" /></td>
<td>Mirrored Care Service</td>
<td>40000+/month Rs</td>
</tr>
<tr>
<td><input class="pricing" name="nutrition_consult" type="checkbox" value="4000" /></td>
<td>Nutrition Consult</td>
<td>4000 Rs</td>
</tr>
<tr>
<td><input class="pricing" name="speedy_service" type="checkbox" value="2000" /></td>
<td>Speedy Service (receive consult within 24 hours)</td>
<td>2000 Rs(extra)</td>
</tr>
<tr>
<td></td>
<td>Total</td>
<td><input class="total_amount" style="border: 0px;" name="total" type="text" value="0" /></td>
</tr>
</tbody>
</table>
<div class="formpaginate" style="overflow:hidden;">
<input class="saveform" style="float: right;" name="price_submit" type="submit" value="Submit" /><input type="button" name="next" class="prev" value="Back" onclick="window.location='http://uat-americandoc.bintg.damcogroup.com/materials-checklist/';" style="float:left; margin-right:10px;"/>
</div>
</form>
                <?php if($enable_comments_page == 1) : ?>
				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>
                <?php endif; ?>
                
            </div><!-- #primary -->
            <div id="pricing-message" style="width:70%;float:right;">Payment Already Done.</div>
            
            <?php if($default_page_layout == 'left_sidebar' || $default_page_layout == 'both_sidebar') : ?>
                <?php get_sidebar('left'); ?>
            <?php endif; ?>
            
        <?php if($default_page_layout == 'both_sidebar') : ?>
            </div> <!-- #primary-wrap -->
        <?php endif; ?>
        
        <?php if($default_page_layout == 'right_sidebar' || $default_page_layout == 'both_sidebar') : ?>
            <?php get_sidebar('right'); ?>
        <?php endif; ?>
    </div>
	</main><!-- #main -->
<?php endwhile; // end of the loop. ?>  	
<?php get_footer(); ?>
