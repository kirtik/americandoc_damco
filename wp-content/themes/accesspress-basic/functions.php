<?php
/**
 * Accesspress Basic functions and definitions
 *
 * @package Accesspress Basic
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'accesspress_basic_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function accesspress_basic_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Accesspress Basic, use a find and replace
	 * to change 'accesspress-basic' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'accesspress-basic', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
    
    /*
     *
     * Add Feature Image Support to the Pages
     *
     */
    add_theme_support( 'post-thumbnails', array( 'page','post','product' ) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'accesspress-basic' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'accesspress_basic_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Woocommerce Compatibility
	add_theme_support( 'woocommerce' );
}
endif; // accesspress_basic_setup
add_action( 'after_setup_theme', 'accesspress_basic_setup' );

/**
 * Enqueue scripts and styles.
 */
function accesspress_basic_scripts() {
	wp_enqueue_style( 'accesspress-basic-superfish-css', get_template_directory_uri() . '/css/superfish.css');
	wp_enqueue_style( 'accesspress-basic-lato-font', '//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' );
	wp_enqueue_style( 'accesspress-basic-style', get_stylesheet_uri() );
	wp_enqueue_style( 'accesspress-basic-responsive-css', get_template_directory_uri() . '/css/responsive.css');
	

	wp_enqueue_script( 'accesspress-basic-superfish', get_template_directory_uri() . '/js/superfish.js', array('jquery','hoverIntent'));
	wp_enqueue_script( 'accesspress-basic-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery'), '20120206', true );

	wp_enqueue_script( 'accesspress-basic-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array('jquery'), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'accesspress_basic_scripts' );

/**
 * Custom Image Sizes
 */
 add_image_size('accesspress-basic-features-post-thumbnail',375,250,true);
 add_image_size('accesspress-basic-testimonial-thumbnail', 125, 125, true);
 add_image_size('accesspress-basic-services-thumbnail', 233, 156, true);
 add_image_size('accesspress-basic-blog-medium-thumbnail', 380, 252, true);
 add_image_size('accesspress-basic-blog-large-thumbnail', 840, 370, true);

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/apbasic-functions.php';

/**
 * Load Accesspress Basic Metaboxes
 */
require get_template_directory() . '/inc/apbasic-custom-metabox.php';

/**
 * Load Theme Options
 */
require get_template_directory() . '/inc/admin-panel/theme-options.php';

/**
 * Load Accesspress Basic Widgets
 */
require get_template_directory() . '/inc/apbasic-widgets.php';

/**
 * Load TGM_Plugin_Activation class.
 */
require get_template_directory() . '/inc/class-tgm-plugin-activation.php';
add_action( 'wp_ajax_nopriv_save_form_data', 'save_form_data' );
add_action( 'wp_ajax_save_form_data', 'save_form_data' );
function save_form_data() { 

  parse_str($_POST[form_values], $searcharray);
$last_insert_id=$searcharray['patientform_id'];
  $user_id=get_current_user_id();
  //echo $user_id;
  $date = $_POST['dob'];
$dd=date_create($date);
$datepic=date_format($dd,"Y-m-d");
global $wpdb;
//$userQuery = $wpdb->get_var("SELECT COUNT(*) FROM wp_patient_info WHERE user_id='".$user_id."'");
    if($last_insert_id == 0) {
          $array1 = serialize(array(array('illness'=>$searcharray['otherillnessname1'],'illdate'=>$searcharray['otherillnessdate1']),array('illness'=>$searcharray['otherillnessname2'],'illdate'=>$searcharray['otherillnessdate2']),array('illness'=>$searcharray['otherillnessname3'],'illdate'=>$searcharray['otherillnessdate3']))); 
          $array2 = serialize(array(array('operation'=>$searcharray['operation1'],'opdate'=>$searcharray['operationdate1']),array('operation'=>$searcharray['operation2'],'opdate'=>$searcharray['operationdate2']),array('operation'=>$searcharray['operation3'],'opdate'=>$searcharray['operationdate3']))); 
          $array3 = serialize(array(array('allergy'=>$searcharray['allergy1'],'opallergy'=>$searcharray['opallergy1']),array('allergy'=>$searcharray['allergy2'],'opallergy'=>$searcharray['opallergy2'])));
          $array4 = serialize(array(array('name'=>$searcharray['name1'],'dose'=>$searcharray['dose1'],'frequency'=>$searcharray['frq1']),array('name'=>$searcharray['name2'],'dose'=>$searcharray['dose2'],'frequency'=>$searcharray['frq2']),array('name'=>$searcharray['name3'],'dose'=>$searcharray['dose3'],'frequency'=>$searcharray['frq3'])));
          $array5 = serialize(array('caffcurrent'=>$searcharray['caffcurrent'],'caffprevious'=>$searcharray['caffprevious'],'cafftype'=>$searcharray['cafftype'],'cafflength'=>$searcharray['cafflength'],'tobcurrent'=>$searcharray['tobcurrent'],'tobprevious'=>$searcharray['tobprevious'],'tobtype'=>$searcharray['tobtype'],'toblength'=>$searcharray['toblength'],'alcurrent'=>$searcharray['alcurrent'],'alprevious'=>$searcharray['alprevious'],'altype'=>$searcharray['altype'],'allength'=>$searcharray['allength'],'drcurrent'=>$searcharray['drcurrent'],'drprevious'=>$searcharray['drprevious'],'drtype'=>$searcharray['drtype'],'drlength'=>$searcharray['drlength']));
          $array6 = serialize(array('fage'=>$searcharray['fage'],'fhealth'=>$searcharray['fhealth'],'faged'=>$searcharray['faged'],'fcause'=>$searcharray['fcause'],'mage'=>$searcharray['mage'],'mhealth'=>$searcharray['mhealth'],'maged'=>$searcharray['maged'],'mcause'=>$searcharray['mcause'],'sibling'=>$searcharray['sibling'],'sage'=>$searcharray['sage'],'shealth'=>$searcharray['shealth'],'saged'=>$searcharray['saged'],'scause'=>$searcharray['scause'],'husband'=>$searcharray['husband'],'hage'=>$searcharray['hage'],'hhealth'=>$searcharray['hhealth'],'haged'=>$searcharray['haged'],'hcause'=>$searcharray['hcause'],'son'=>$searcharray['son'],'soage'=>$searcharray['soage'],'sohealth'=>$searcharray['sohealth'],'soaged'=>$searcharray['soaged'],'socause'=>$searcharray['socause']));
          $array7 = serialize(array('cancer'=>$searcharray['cancer'],'breast'=>$searcharray['breast'],'colon'=>$searcharray['colon'],'ovary'=>$searcharray['ovary'],'uterus'=>$searcharray['uterus'],'stroke'=>$searcharray['stroke'],'HBP'=>$searcharray['HBP'],'triglycerides'=>$searcharray['triglycerides'],'heart'=>$searcharray['heart'],'diabetes'=>$searcharray['diabetes'],'ulcer'=>$searcharray['ulcer'],'liver'=>$searcharray['liver'],'kidney'=>$searcharray['kidney'],'lung'=>$searcharray['lung'],'genetic'=>$searcharray['genetic'],'epilepsy'=>$searcharray['epilepsy'],'migraine'=>$searcharray['migraine'],'mental'=>$searcharray['mental'],'alcohol'=>$searcharray['alcohol'],'goiter'=>$searcharray['goiter'],'arthritis'=>$searcharray['arthritis'],'crohn'=>$searcharray['crohn'],'bowel'=>$searcharray['bowel']));
          $query="INSERT INTO wp_patient_info( 
user_id,last_updated_time,pname,gender,dob,mother_name,father_name,phone_no,contact_work,email,address,current_diagnosis,hope_from_consult,question_by_consult,name_primary_care_physician,physician_address,physician_phone,physician_fax,physician_email,if_other_physician,like_to_send_consult,primary_medical_problem,if_rheumatic_fever,if_measles,if_mumps,if_heart_murmur,if_anemia,if_hepatitis,if_diabetes,if_heart_attack,if_lung_disease,if_kidney_disease,if_cancer,if_stroke,if_tuberculosis,if_mental_disease,if_other_illness,if_any_surgery,if_any_allergy_to_medications,current_medications,if_stool_tested_for_blood,if_colonoscopy,if_blood_cholesterol_level,if_psa,if_chest_xray,if_ecg,if_mammogram,if_pap_pelvic,if_ctscan_abdomen,if_liver_biopsy,if_poor_appetite,if_weight_loss,if_weight_gain,if_easy_fatigability,if_transfusion,if_anxiety,if_fever,if_itching,if_depression,if_eye_trouble,if_hearing_disorder,if_sore_tongue,if_yellow_eyes,if_goiter,if_lumps,if_chest_pain,if_shortnessof_breath,if_palpitations,if_asthma,if_chronic_cough,if_high_bp,if_heart_burn,if_difficulty_swallowing,if_indigestion,if_milk_intolerance,if_persistent_nausea,if_vomiting_blood,if_passing_blood,if_abdominal_pain,if_diarrhea,if_constipation,if_abdominal_swelling,if_difficulty_with_urination,if_blood_in_urine,if_dark_urine,if_kidney_stones,if_difficulty_with_MP,last_menstrual_period,if_contraceptive_use,if_estrogen_replacement,if_arthritis,if_swollen_legs,if_cold_sensitivity,if_recurrent_headaches,if_loss_of_consciousness,if_seizures,if_loss_of_memory,if_confusion,if_tremor,if_weakness ,current_weight,lowest_weight,highest_weight,if_influenza,if_tetanus,if_pneumonia,if_hepatitisB,years_of_education,occupation,current_employment_status,current_occupation,if_disabled,disabled_cause,if_abused,marital_status,current_spouse,spouse_employment_status,spouse_current_occupation,do_you_excercise,if_difficulty_asleep,if_awaken_early,used_substances,family_history,blood_relative) 
VALUES (
'$user_id','now()','".$searcharray['pname']."','".$searcharray["sex"]."','$datepic','".$searcharray["mother_name"]."','".$searcharray["father_name"]."','".$searcharray["phone_no"]."','".$searcharray["contact_work"]."','".$searcharray["email"]."','".$searcharray["address"]."','".$searcharray["current_diagnosis"]."','".$searcharray["hope_from_consult"]."','".$searcharray["question_by_consult"]."','".$searcharray["name_primary_care_physician"]."','".$searcharray["physician_address"]."',
'".$searcharray["physician_phone"]."','".$searcharray["physician_fax"]."','".$searcharray["physician_email"]."','".$searcharray["other_physician"]."','".$searcharray["like_to_send_consult"]."','".$searcharray["primary_medical_problem"]."','".$searcharray["rheumatic_fever"]."','".$searcharray["measles"]."',
'".$searcharray["mumps"]."','".$searcharray["heart_murmur"]."','".$searcharray["anemia"]."','".$searcharray["hepatitis"]."','".$searcharray["diabetes"]."','".$searcharray["heart_attack"]."','".$searcharray["lung_disease"]."','".$searcharray["kidney_disease"]."','".$searcharray["cancer"]."','".$searcharray["stroke"]."','".$searcharray["tuberculosis"]."',
'".$searcharray["mental_disease"]."','".$array1."','".$array2."','".$array3."','".$array4."',
'".$searcharray["stool_test"]."','".$searcharray["colonoscopy"]."','".$searcharray["blood_cholesterol_level"]."','".$searcharray["psa"]."','".$searcharray["chest_xray"]."','".$searcharray["ecg"]."','".$searcharray["mammogram"]."','".$searcharray["pap_pelvic"]."',
'".$searcharray["ctscan_abdomen"]."','".$searcharray["liver_biopsy"]."','".$searcharray["poor_appetite"]."','".$searcharray["weight_loss"]."','".$searcharray["weight_gain"]."','".$searcharray["easy_fatigability"]."','".$searcharray["transfusion"]."',
'".$searcharray["anxiety"]."','".$searcharray["fever"]."','".$searcharray["itching"]."','".$searcharray["depression"]."','".$searcharray["eye_trouble"]."','".$searcharray["hearing_disorder"]."','".$searcharray["sore_tongue"]."','".$searcharray["yellow_eyes"]."','".$searcharray["goiter"]."',
'".$searcharray["lumps"]."','".$searcharray["chest_pain"]."','".$searcharray["shortnessof_breath"]."','".$searcharray["palpitations"]."','".$searcharray["asthma"]."','".$searcharray["chronic_cough"]."','".$searcharray["high_bp"]."','".$searcharray["heart_burn"]."',
'".$searcharray["difficulty_swallowing"]."','".$searcharray["indigestion"]."','".$searcharray["milk_intolerance"]."','".$searcharray["persistent_nausea"]."','".$searcharray["vomiting_blood"]."','".$searcharray["passing_blood"]."','".$searcharray["abdominal_pain"]."','".$searcharray["diarrhea"]."','".$searcharray["constipation"]."','".$searcharray["abdominal_swelling"]."','".$searcharray["urination"]."','".$searcharray["blood_in_urine"]."','".$searcharray["dark_urine"]."','".$searcharray["kidney_stones"]."','".$searcharray["difficulty_with_MP"]."','".$searcharray["last_menstrual_period"]."','".$searcharray["contraceptive_use"]."','".$searcharray["estrogen_replacement"]."','".$searcharray["arthritis"]."','".$searcharray["swollen_legs"]."','".$searcharray["cold_sensitivity"]."','".$searcharray["recurrent_headaches"]."','".$searcharray["loss_of_consciousness"]."','".$searcharray["seizures"]."','".$searcharray["loss_of_memory"]."','".$searcharray["confusion"]."','".$searcharray["tremor"]."','".$searcharray["weakness "]."','".$searcharray["current_weight"]."','".$searcharray["lowest_weight"]."','".$searcharray["highest_weight"]."','".$searcharray["influenza"]."','".$searcharray["tetanus"]."','".$searcharray["pneumonia"]."','".$searcharray["hepatitisB"]."','".$searcharray["years_of_education"]."','".$searcharray["occupation"]."','".$searcharray["current_employment_status"]."','".$searcharray["current_occupation"]."','".$searcharray["disabled"]."','".$searcharray["disabled_cause"]."','".$searcharray["abused"]."','".$searcharray["marital_status"]."','".$searcharray["current_spouse"]."','".$searcharray["spouse_employment_status"]."','".$searcharray["spouse_current_occupation"]."','".$searcharray["exercise"]."','".$searcharray["sleep"]."','".$searcharray["awaken_early"]."','".$array5."','".$array6."','".$array7."')";

        global $wpdb;
$result=$wpdb->query($query);
if($result){
$get_insert_id= $wpdb->insert_id;
echo $get_insert_id;
}
else
{
    echo 'Error!';
}
	
	 }
         else
         {
             $query="UPDATE wp_patient_info SET user_id = '$user_id',
                                                last_updated_time='now()',
                                                pname = '".$searcharray['pname']."',
                                                 gender = '".$searcharray["sex"]."', 
                                                dob = '".$searcharray["dob"]."',
                                                mother_name ='".$searcharray["mother_name"]."',
                                                father_name='".$searcharray["father_name"]."',
                                                phone_no='".$searcharray["phone_no"]."' ,
                                                contact_work='".$searcharray["contact_work"]."',
                                                email='".$searcharray["email"]."',
                                                address='".$searcharray["address"]."',
                                                current_diagnosis='".$searcharray["current_diagnosis"]."',
                                                hope_from_consult='".$searcharray["hope_from_consult"]."',
                                                question_by_consult='".$searcharray["question_by_consult"]."',
                                                name_primary_care_physician='".$searcharray["name_primary_care_physician"]."',
                                                physician_address='".$searcharray["physician_address"]."',
                                                physician_phone ='".$searcharray["physician_phone"]."' ,
                                                physician_fax ='".$searcharray["physician_fax"]."' ,
                                                physician_email ='".$searcharray["physician_email"]."' ,
                                                if_other_physician ='".$searcharray["other_physician"]."' ,
                                                like_to_send_consult='".$searcharray["like_to_send_consult"]."' ,
                                                primary_medical_problem='".$searcharray["primary_medical_problem"]."' ,
                                                if_rheumatic_fever ='".$searcharray["rheumatic_fever"]."' ,
                                                if_measles ='".$searcharray["measles"]."',
                                                if_mumps ='".$searcharray["mumps"]."' ,
                                                if_heart_murmur ='".$searcharray["heart_murmur"]."' ,
                                                if_anemia ='".$searcharray["anemia"]."' ,
                                                if_hepatitis ='".$searcharray["hepatitis"]."' ,
                                                if_diabetes ='".$searcharray["diabetes"]."' ,
                                                if_heart_attack='".$searcharray["heart_attack"]."'
                                                if_lung_disease ='".$searcharray["lung_disease"]."' ,
                                                if_kidney_disease ='".$searcharray["kidney_disease"]."' ,
                                                if_cancer ='".$searcharray["cancer"]."' ,
                                                if_stroke = '".$searcharray["stroke"]."',
                                                if_tuberculosis ='".$searcharray["tuberculosis"]."' ,
                                                if_mental_disease ='".$searcharray["mental_disease"]."' ,
                                                if_other_illness ='".$array1."' ,
                                                if_any_surgery ='".$array2."' ,
                                                if_any_allergy_to_medications='".$array3."' ,
                                                current_medications ='".$array4."' ,
                                                if_stool_tested_for_blood ='".$searcharray["stool_test"]."' ,
                                                if_colonoscopy = '".$searcharray["colonoscopy"]."',
                                                if_blood_cholesterol_level = '".$searcharray["blood_cholesterol_level"]."',
                                                if_psa ='".$searcharray["psa"]."' ,
                                                if_chest_xray ='".$searcharray["chest_xray"]."' ,
                                                if_ecg = '".$searcharray["ecg"]."',
                                                if_mammogram ='".$searcharray["mammogram"]."' ,
                                                if_pap_pelvic = '".$searcharray["pap_pelvic"]."',
                                                if_ctscan_abdomen ='".$searcharray["ctscan_abdomen"]."' ,
                                                if_liver_biopsy ='".$searcharray["liver_biopsy"]."' ,
                                                if_poor_appetite = '".$searcharray["poor_appetite"]."',
                                                if_weight_loss ='".$searcharray["weight_loss"]."' ,
                                                if_weight_gain ='".$searcharray["weight_gain"]."' ,
                                                if_easy_fatigability ='".$searcharray["easy_fatigability"]."' ,
                                                if_transfusion='".$searcharray["transfusion"]."' ,
                                                if_anxiety='".$searcharray["anxiety"]."',
                                                if_fever='".$searcharray["fever"]."',
                                                if_itching='".$searcharray["itching"]."',
                                                if_depression='".$searcharray["depression"]."',
                                                if_eye_trouble='".$searcharray["eye_trouble"]."',
                                                if_hearing_disorder='".$searcharray["hearing_disorder"]."',
                                                if_sore_tongue='".$searcharray["sore_tongue"]."',
                                                if_yellow_eyes='".$searcharray["yellow_eyes"]."',
                                                if_goiter='".$searcharray["goiter"]."',
                                                if_lumps='".$searcharray["lumps"]."',
                                                if_chest_pain='".$searcharray["chest_pain"]."',
                                                if_shortnessof_breath='".$searcharray["shortnessof_breath"]."',
                                                if_palpitations='".$searcharray["palpitations"]."',
                                                if_asthma='".$searcharray["asthma"]."',
                                                if_chronic_cough='".$searcharray["chronic_cough"]."',
                                                if_high_bp='".$searcharray["high_bp"]."',
                                                if_heart_burn='".$searcharray["heart_burn"]."',
                                                if_difficulty_swallowing='".$searcharray["difficulty_swallowing"]."',
                                                if_indigestion='".$searcharray["indigestion"]."',
                                                if_milk_intolerance='".$searcharray["milk_intolerance"]."',
                                                if_persistent_nausea='".$searcharray["persistent_nausea"]."',
                                                if_vomiting_blood='".$searcharray["vomiting_blood"]."',
                                                if_passing_blood='".$searcharray["passing_blood"]."',
                                                if_abdominal_pain='".$searcharray["abdominal_pain"]."',
                                                if_diarrhea='".$searcharray["diarrhea"]."',
                                                if_constipation='".$searcharray["constipation"]."',
                                                if_abdominal_swelling='".$searcharray["abdominal_swelling"]."',
                                                if_difficulty_with_urination='".$searcharray["urination"]."',
                                                if_blood_in_urine='".$searcharray["blood_in_urine"]."',
                                                if_dark_urine='".$searcharray["dark_urine"]."',
                                                if_kidney_stones='".$searcharray["kidney_stones"]."',
                                                if_difficulty_with_MP='".$searcharray["difficulty_with_MP"]."',
                                                last_menstrual_period='".$searcharray["last_menstrual_period"]."',
                                                if_contraceptive_use='".$searcharray["contraceptive_use"]."',
                                                if_estrogen_replacement='".$searcharray["estrogen_replacement"]."',
                                                if_arthritis='".$searcharray["arthritis"]."',
                                                if_swollen_legs='".$searcharray["swollen_legs"]."',
                                                if_cold_sensitivity='".$searcharray["cold_sensitivity"]."',
                                                if_recurrent_headaches='".$searcharray["recurrent_headaches"]."',
                                                if_loss_of_consciousness='".$searcharray["loss_of_consciousness"]."',
                                                if_seizures='".$searcharray["seizures"]."',
                                                if_loss_of_memory='".$searcharray["loss_of_memory"]."',
                                                if_confusion='".$searcharray["confusion"]."',
                                                if_tremor='".$searcharray["tremor"]."',
                                                if_weakness='".$searcharray["weakness "]."' ,
                                                current_weight='".$searcharray["current_weight"]."',
                                                lowest_weight='".$searcharray["lowest_weight"]."',
                                                highest_weight='".$searcharray["highest_weight"]."',
                                                if_influenza='".$searcharray["influenza"]."',
                                                if_tetanus='".$searcharray["tetanus"]."',
                                                if_pneumonia='".$searcharray["pneumonia"]."',
                                                if_hepatitisB='".$searcharray["hepatitisB"]."',
                                                years_of_education='".$searcharray["years_of_education"]."',
                                                occupation='".$searcharray["occupation"]."',
                                                current_employment_status='".$searcharray["current_employment_status"]."',
                                                current_occupation='".$searcharray["current_occupation"]."',
                                                if_disabled='".$searcharray["disabled"]."',
                                                disabled_cause='".$searcharray["disabled_cause"]."',
                                                if_abused='".$searcharray["abused"]."',
                                                marital_status='".$searcharray["marital_status"]."',
                                                current_spouse='".$searcharray["current_spouse"]."',
                                                spouse_employment_status='".$searcharray["spouse_employment_status"]."',
                                                spouse_current_occupation='".$searcharray["spouse_current_occupation"]."',
                                                do_you_excercise='".$searcharray["do_you_excercise"]."',
                                                if_difficulty_asleep='".$searcharray["difficulty_asleep"]."',
                                                if_awaken_early='".$searcharray["awaken_early"]."',
                                                used_substances ='".$array5."',
                                                family_history ='".$array6."',
                                                blood_relative ='".$array7."' ";
              global $wpdb;
$result1=$wpdb->query($query);
if($result1){
$get_insert_id= $wpdb->insert_id;
echo $get_insert_id;
         }}
         die();
}