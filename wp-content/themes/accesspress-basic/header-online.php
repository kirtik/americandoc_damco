<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Accesspress Basic
 */
?><!DOCTYPE html>
<?php
    global $apbasic_options;
    $apbasic_settings = get_option('apbasic_options',$apbasic_options);
    if ( is_array( $apbasic_settings ) && ! empty( $apbasic_settings )) {
        extract($apbasic_settings);
    }
    
    $site_class = null;
    if($site_layout == 'boxed'){
        $site_class = 'boxed-layout';
    }

    $header_class = '';
    switch($show_header){
        case 'header_logo_only' :
            $header_class = 'header-logo-only';
            break;
        case 'header_text_only' :
            $header_class = 'header-text-only';
            break;
        case 'show_both' :
            $header_class = 'header-text-logo';
            break;
    }
?>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  

  
 
 
<?php if(!empty($favicon)) : ?>
    <?php if($activate_favicon == 1) : ?>
        <link rel="icon" type="image/png" href="<?php echo esc_url($favicon); ?>">
    <?php endif; ?>
<?php endif; ?>




<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type='text/javascript'>
$(document).ready(function (){
<?php if(is_page('pricing')) { ?>
$('#secondary #text-5').hide();
$('#secondary #text-6').show();



<?php } ?>
<?php if(is_page('request-consult')||is_page('information')||is_page('online-questionnare')||is_page('material-checklists')||is_page('general-information')||is_page('medical-history-questionnaire-part-1')||is_page('medical-history-questionnaire-part-2')||is_page('medical-history-questionnaire-part-3')) { ?>
$('#secondary #text-6').hide();
$('#secondary #text-5').show();
$('#secondary #text-7').hide();

<?php } ?>

});
</script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
$(document).ready(function(){
   
    $( "#dob" ).datepicker();
  
 $('.saveform').click(function(){       
    var formvalues=$('#online').serialize()
   var data = {
			'action': 'save_form_data',
			'form_values': formvalues
		};
   var url  = '<?php echo admin_url('admin-ajax.php'); ?>';
   $.ajax({
    type:  'POST',
    dataType:  'JSON',
    url:  url,
    data:   data,
    success:  function(dataReturn){
	//alert(dataReturn);
        $('#patientform_id').val(dataReturn);
		 $('#loadingmessage').hide();
            
            alert('form was submitted');
			 window.location = "http://uat-americandoc.bintg.damcogroup.com/materials-checklist/";}
            
    }
   );  
   return false; 
  });
 
 });
</script>


<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url')?>/css/formwizard.css" />

<script src="<?php bloginfo('template_url')?>/js/formwizard.js" type="text/javascript">

/***********************************************
* jQuery Form to Form Wizard- (c) Dynamic Drive (www.dynamicdrive.com)
* Please keep this notice intact
* Visit http://www.dynamicdrive.com/ for this script and 100s more.
***********************************************/

</script>

<script type="text/javascript">

var myform=new formtowizard({
	formid: 'feedbackform',
	persistsection: true,
	
	revealfx: ['slide', 500]
})

</script>



<script type="text/javascript">

var myform=new formtowizard({
	formid: 'online',
	persistsection: true,
	revealfx: ['slide', 500]
})

</script>
  
  
  
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
	<!--<script src="<?php bloginfo('template_url') ?>/js/formvalidation.js"></script>-->

<script src="http://jquery.bassistance.de/validate/additional-methods.js"></script>
<script>
$( "#saveform" ).click(function(){
$('input[type="checkbox"]').click(function () {
    $(this).prop("checked") ? $(this).val("Yes") : $(this).val("No")
});
});
</script>
<?php wp_head(); ?>
</head>

<body <?php body_class($site_class); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'accesspress-basic' ); ?></a>

	<header id="masthead" class="site-header <?php echo $header_class; ?>" role="banner">
        	<div class="top-header clearfix">
                <div class="ap-container">
                    <div class="site-branding">
                        <?php if($show_header != 'disable') : ?>
                            
                            <?php if($show_header == 'header_logo_only') : ?>
                                <?php if(get_header_image()) : ?>
                                    <div class="header-logo-container">
                                        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo header_image(); ?>" /></a></h1>
                                    </div>
                                <?php endif; ?>
                            <?php elseif($show_header == 'header_text_only') : ?>
                                <div class="header-text-container">
                        			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                        			<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
                                </div>
                            <?php else : ?>
                                <?php if(get_header_image()) : ?>
                                    <div class="header-logo-container">
                                        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo $header_logo; ?>" /></a></h1>
                                    </div>
                                <?php endif; ?>
                                <div class="header-text-container">
                        			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                        			<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
                                </div>
                            <?php endif; ?>
                            
                        <?php endif; ?>
            		</div><!-- .site-branding -->
                    <div class="right-top-head">
                        <?php if(is_active_sidebar('apbasic_header_text')) : ?>
                            <div class="call-us"><?php dynamic_sidebar('apbasic_header_text'); ?></div>
                        <?php else : ?>
                            <?php if(!empty($header_text)) : ?>
                                <div class="call-us"><?php echo esc_attr($header_text); ?></div>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if($show_social_links == 1 && is_active_sidebar('apbasic_header_social_links')) : ?>
                        <div class="social-icons-head">
                            <div class="social-container">
                                <?php dynamic_sidebar('apbasic_header_social_links'); ?>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div> <!-- ap-container -->
            </div> <!-- top-header -->
            
            <div class="menu-wrapper clearfix"> 
                <div class="ap-container">
                    <a class="menu-trigger"><span></span><span></span><span></span></a>   
            		<nav id="site-navigation" class="main-navigation" role="navigation">
            			<button class="menu-toggle hide" aria-controls="primary-menu" aria-expanded="false"><?php _e( 'Primary Menu', 'accesspress-basic' ); ?></button>
            			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
            		</nav><!-- #site-navigation -->
                    <?php if($show_search == 1) : ?>
                        <div class="search-icon">
                        <i class="fa fa-search"></i>
                        <div class="ak-search">
                            <div class="close">&times;</div>
                                 <form action="<?php echo site_url(); ?>" class="search-form" method="get" role="search">
                                    <label>
                                        <span class="screen-reader-text"><?php _e('Search for:', 'accesspress-basic'); ?></span>
                                        <input type="search" title="Search for:" name="s" value="" placeholder="<?php _e('Search content...', 'accesspress-basic'); ?>" class="search-field">
                                    </label>
                                    <input type="submit" value="Search" class="search-submit">
                                 </form>
                         <div class="overlay-search"> </div> 
                        </div>
                    </div> 
                <?php endif; ?>
                </div>
            </div>
            <nav id="site-navigation-responsive" class="main-navigation-responsive">
    			<button class="menu-toggle hide" aria-controls="primary-menu" aria-expanded="false"><?php _e( 'Primary Menu', 'accesspress-basic' ); ?></button>
    			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
    		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
    <?php
        if($show_slider == 'yes') :
            if($show_slider_in_post == 1) :
                 if(is_front_page() || is_single()) :
                 ?>
                <div class="ap-basic-slider-wrapper">
                <div class="ap-container">
                 <?php 
                    do_action('accesspress_basic_slider');
                ?>
                </div>
                </div>
                <?php
                 endif;
            else:
                if(is_front_page()) :
                ?>
                <div class="ap-basic-slider-wrapper">
                <div class="ap-container">
                <?php
                    do_action('accesspress_basic_slider');
                ?>
                </div>
                </div>
                <?php
                endif;
            endif;
        endif;
    ?>
	
	<?php
 if( !is_user_logged_in() ) {
  wp_redirect('http://uat-americandoc.bintg.damcogroup.com/pie-register-login/');
  exit;
 }
?>
    