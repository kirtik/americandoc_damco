<?php
/**
 *Template Name: Online Questions
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Accesspress Basic
 */
// if($_POST){
// echo "11";
// die();
// }
global $apbasic_options;
$apbasic_settings = get_option('apbasic_options', $apbasic_options);
if ( is_array( $apbasic_settings ) && ! empty( $apbasic_settings )) {
    extract($apbasic_settings);
}
global $wpdb;
$iduser = get_current_user_id();
$formData = $wpdb->get_row("SELECT * FROM wp_patient_info WHERE user_id ='".$iduser."'");
get_header('online'); ?>

<?php 
    $single_default_page_layout = get_post_meta( $post->ID, 'apbasic_page_layout', true);
    $default_page_layout = ($single_default_page_layout == 'default_layout') ? $default_page_layout : $single_default_page_layout;
    
    // Dynamically Generating Classes for #primary on the basis of page layout
    $content_class = '';
    switch($default_page_layout){
        case 'left_sidebar':
            $content_class = 'left-sidebar';
            break;
        case 'right_sidebar':
            $content_class = 'right-sidebar';
            break;
        case 'both_sidebar':
            $content_class = 'both-sidebar';
            break;
        case 'no_sidebar_wide':
            $content_class = 'no-sidebar-wide';
            break;
        case 'no_sidebar_narrow':
            $content_class = 'no-sidebar-narraow';
            break;
    }
?>
<?php while ( have_posts() ) : the_post(); ?>
	<main id="main" class="site-main <?php echo $content_class; ?>" role="main">
        <div class="ap-container">
        <?php if($default_page_layout == 'both_sidebar') : ?>
            <div id="primary-wrap" class="clearfix">
        <?php endif; ?> 
        
            <div id="primary" class="content-area">     
	
				<form id="online" name="online" method="POST" action="<?php //bloginfo('template_url'); ?>">
				
                                    <input type="hidden" name="patientform_id" id="patientform_id" value="0">
				<fieldset class="sectionwrap">
				
                                        <label class="info-label">Name</label> <input  class="info-value" id="pname" name="pname" type="text" <?php if(!empty($formData->pname)){echo "Value=".$formData->pname;} else {echo 'Placeholder="Enter your name"';} ?> <?php if(!empty($formData->pname)){echo "readonly";} ?> />
					<label class="info-label">Gender </label>
                                        <input  <?php if( $formData->gender ){ if($formData->gender === 'Male'){echo "checked"; }else{?> disabled="disabled" <?php } }?> name="sex" type="radio" value="male" />Male
					<input <?php  if( $formData->gender ){ if($formData->gender === 'female'){echo "checked"; }else{?> disabled="disabled" <?php }}?> name="sex" type="radio" value="female" />Female
					<div style="clear:both"></div>
					<label class="info-label"> Date of Birth </label> 
                                        <?php
                                            if(!empty($formData->dob)){
                                        ?>
                                        <input class="info-value" id="dob" name="dob" type="text" value="<?php echo $formData->dob;?>" readonly/>
                                        <?php } else { ?>
                                        <input class="info-value" id="dob" name="dob" type="text" id="datepicker" placeholder=""/>
                                        <?php } ?>
					<div style="clear: both;"></div>
If Requestor isn't patient or patient is under age 18, please submit <a href="http://localhost/americandoc/information/">Proxy Consent Form </a>(barring extenuating circumstances)
<div style="clear:both"></div>
<label class="info-label">Mother's Maiden Name</label> <input class="info-value" id="mother_name" name="mother_name" type="text" <?php if(!empty($formData->mother_name)){echo "Value=".$formData->mother_name;} else {echo "Placeholder="."Enter Mother's Maiden Name";} ?> <?php if(!empty($formData->mother_name)){echo "readonly";} ?>/>
<label class="info-label">Father's Full Name</label> <input class="info-value" id="father_name" name="father_name" type="text" <?php if(!empty($formData->father_name)){echo "Value=".$formData->father_name;} else {echo "Placeholder="."Enter Father's Full Name";} ?> <?php if(!empty($formData->father_name)){echo "readonly";} ?>/>
<label class="info-label">Phone No.</label> <input class="info-value" name="phone_no" type="text" <?php if(!empty($formData->phone_no)){echo "Value=".$formData->phone_no;} else {echo "Placeholder="."Enter Phone Number";} ?> <?php if(!empty($formData->phone_no)){echo "readonly";} ?>/>
<label class="info-label">Is it ok to contact you at work? Y/N</label>
<?php
    if(!empty($formData->contact_work)){
?>
    <input class="info-value" type ="text" name="contact_work" Value="<?php echo $formData->contact_work;?>" readonly>
    <?php } else {?>
     <select class="info-value" name="contact_work">
        <option value="yes">Yes</option>
        <option value="no">No</option>
     </select>    
    <?php } ?>

<div style="clear: both;"></div>
<label class="info-label">Email</label> <input class="info-value" type="text" name="email" <?php if(!empty($formData->email)){echo "Value=".$formData->email;} else {echo "placeholder="."Enter Email";} ?> <?php if(!empty($formData->email)){echo "readonly";} ?>>
<label class="info-label">Address</label> <input class="info-value" type="text" name="address" <?php if(!empty($formData->address)){echo "Value=".$formData->address;} else {echo "placeholder="."Enter Address";} ?> <?php if(!empty($formData->address)){echo "readonly";} ?>>
<?php
    if(!empty($formData->type_of_consult)){
?>
 <label class="info-label">Type of Consult</label> <input class="info-value" type ="text" name="type_of_consult" Value="<?php echo $formData->type_of_consult;?>" readonly>
    <?php } else {?>
<label class="info-label">Type of Consult</label> <select class="info-value" name="type_of_consult">
<option value="single_specialty_consult">Single Specialty Consults</option>
<option value="multi_specialty_consult"> Multi Specialty Consults </option>
<option value="orthopedic_consult">Orthopedic/Cardiac Surgeon Consult</option>
<option value="diabetes_consult">Diabetes Consult</option>
<option value="mirrored_care_service">Mirrored Care Service</option>
<option value="nutrition_consult">Nutrition Consult</option>
</select>
<?php } ?>
<div style="clear: both;"></div>
<label class="info-label">What is your current diagnosis/disease/disorder?</label> <input type="textarea" name="current_diagnosis" class="info-value" <?php if(!empty($formData->current_diagnosis)){echo "Value=".$formData->current_diagnosis;}else{echo "";}?> <?php if(!empty($formData->current_diagnosis)){echo "readonly";}?>>
<label class="info-label">What do you hope to gain by engaging the services of an AmericanDoc Consult?</label> 
<input  type="textarea" name="hope_from_consult" class="info-value" <?php if(!empty($formData->hope_from_consult)){echo "Value=".$formData->hope_from_consult;}else{echo "";}?> <?php if(!empty($formData->hope_from_consult)){echo "readonly";}?>>
<label class="info-label">What questions do you want answered by your AmericanDoc Consult? </label>
 <input  type="textarea" name="question_by_consult" class="info-value" <?php if(!empty($formData->question_by_consult)){echo "Value=".$formData->question_by_consult;}else{echo "";}?> <?php if(!empty($formData->question_by_consult)){echo "readonly";}?>>
<div style="clear: both;"></div>
<input class="saveform" type="submit" name="Save" id="saveform" value="Save" style="float:right;" >

</fieldset>


<fieldset class="sectionwrap">

<h5>Please include all information that can be obtained</h5>
<strong> Primary Care Physician Information (Doctor who is in charge of your diagnosis/treatment)</strong>
<label class="info-label">Name of Primary Care Physician:</label> <input class="info-value" name="name_primary_care_physician" type="text" <?php if(!empty($formData->name_primary_care_physician)){echo "Value=".$formData->name_primary_care_physician;}else{echo "Placeholder="."Name of Primary Care Physician";}?> <?php if(!empty($formData->name_primary_care_physician)){echo "readonly";}?> />
<label class="info-label">Physician's Address:</label> <input class="info-value" name="physician_address" type="text" <?php if(!empty($formData->physician_address)){echo "Value=".$formData->physician_address;}else{echo "Placeholder="."Physician Address";}?> <?php if(!empty($formData->physician_address)){echo "readonly";}?> />
<label class="info-label">Physician's Phone:</label> <input class="info-value" name="physician_phone" type="text" <?php if(!empty($formData->physician_phone)){echo "Value=".$formData->physician_phone;}else{echo "Placeholder="."Phone Number";}?> <?php if(!empty($formData->physician_phone)){echo "readonly";}?> />
<label class="info-label">Physician's Fax:</label> <input class="info-value" name="physician_fax" type="text" <?php if(!empty($formData->physician_fax)){echo "Value=".$formData->physician_fax;}else{echo "Placeholder="."Fax";}?> <?php if(!empty($formData->physician_fax)){echo "readonly";}?>/>
<label class="info-label">Physician's Email:</label> <input class="info-value" name="physician_email" type="text" <?php if(!empty($formData->physician_email)){echo "Value=".$formData->physician_email;}else{echo "Placeholder"."Physician Email";}?> <?php if(!empty($formData->physician_email)){echo "readonly";}?>/>
<label class="info-label">Other Physicians involved with your care:</label> <input class="info-value" name="other_physician" type="text" <?php if(!empty($formData->if_other_physician)){echo "Value=".$formData->if_other_physician;}else{echo "Placeholder="."Other Physician";}?> <?php if(!empty($formData->if_other_physician)){echo "readonly";}?>/>
<label class="info-label">Would you like us to send your consult to your primary care physician? Y/N </label>
<?php
    if(!empty($formData->like_to_send_consult)){
?>
    <input class="info-value" type="text" name="like_to send_consult" Value="<?php if(!empty($formData->like_to_send_consult)){echo $formData->like_to_send_consult;}else{echo "";}?>" <?php if(!empty($formData->like_to_send_consult)){echo "readonly";}?> > 
    <?php } else { ?>
<select class="info-value" name="like_to send_consult">
<option value="yes">Yes</option>
<option value="no">No</option>
</select>
    <?php } ?>
<label class="info-label">What is the primary medical problem for which you seek evaluation information or treatment?</label> <input class="info-value" name="primary_medical_problem" type="text" <?php if(!empty($formData->primary_medical_problem)){echo "Value=".$formData->primary_medical_problem;}else{echo "";}?> <?php if(!empty($formData->primary_medical_problem)){echo "readonly";}?>/>
<br/>
<div style="clear: both;"></div>
<input class="saveform" type="submit" name="Save" id="saveform" value="Save" style="float:right;" >

</fieldset>

<fieldset class="sectionwrap">
<label>Indicate if you have had any of the following :</label><br/>
<div style="float: left;"><input name="rheumatic_fever" type="checkbox" value="Yes" <?php if($formData->if_rheumatic_fever == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Rheumatic fever<br/>
<input name="measles" type="checkbox" value="Yes" <?php if($formData->if_measles == 'yes'){echo 'checked'; ?> onclick="return false;"<?php }?>/> Measles<br/>
<input name="mumps" type="checkbox" value="Yes" <?php if($formData->if_mumps == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Mumps<br/>
<input name="heart_murmur" type="checkbox" value="Yes" <?php if($formData->if_heart_murmur == 'yes'){echo 'checked'; ?> onclick="return false;"<?php }?>/> Heart murmur<br/>
<input name="anemia" type="checkbox" value="Yes" <?php if($formData->if_anemia == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Anemia<br/>
<input name="hepatitis" type="checkbox" value="Yes" <?php if($formData->if_hepatitis == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Hepatitis<br/>
<input name="diabeteschk" type="checkbox" value="Yes" <?php if($formData->if_diabetes == 'Yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Diabetes<br/>
<input name="heart_attack" type="checkbox" value="Yes" <?php if($formData->if_heart_attack == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Heart attack<br/></div>
<div style="float: left; padding-left: 100px;"><input name="lung_disease" type="checkbox" value="Yes" <?php if($formData->if_lung_disease == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Lung disease<br/>
<input name="kidney_disease" type="checkbox" value="Yes" <?php if($formData->if_kidney_disease == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Kidney disease<br/>
<input name="cancerd" type="checkbox" value="Yes" <?php if($formData->if_cancer == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Cancer<br/>
<input name="stroked" type="checkbox" value="Yes" <?php if($formData->if_stroke == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Stroke<br/>
<input name="tuberculosis" type="checkbox" value="Yes" <?php if($formData->if_tuberculosis == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Tuberculosis<br/>
<input name="mental_disease" type="checkbox" value="Yes" <?php if($formData->if_mental_disease == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Mental Disease<br/></div>
<div style="clear: both;"></div>
<label>What other illness have you had?</label>
<table style="height: 5px; width: 711px;">
<tbody>
<tr>
<td>Name</td>
<td>Approximate Date</td>
</tr>
<?php if(!empty($formData->if_other_illness)){
        $other_illness = unserialize($formData->if_other_illness);
        $count1 = 1;
        foreach($other_illness as $illness){
            ?>
<tr>
<td><input type="text" name="otherillnessname<?php echo $count1;?>" value="<?php echo $illness['illness'];?>" readonly /></td>
<td><input type="text" name="otherillnessdate<?php echo $count1;?>" value="<?php echo $illness['illdate'];?>" readonly/></td>
</tr>
        <?php $count1++;}
} else {
    ?>
<tr>
<td><input type="text" name="otherillnessname1" /></td>
<td><input type="text" name="otherillnessdate1" /></td>
</tr>
<tr>
<td><input type="text" name="otherillnessname2" /></td>
<td><input type="text" name="otherillnessdate2" /></td>
</tr>
<tr>
<td><input type="text" name="otherillnessname3" /></td>
<td><input type="text" name="otherillnessdate3" /></td>
</tr>
<?php } ?>
</tbody>
</table>
<label>Have you had any surgery?</label>
<table style="height: 5px; width: 711px;">
<tbody>
<tr>
<td>Type of operation</td>
<td>Approximate Date</td>
</tr>
<?php if(!empty($formData->if_any_surgery)){
        $any_surgery = unserialize($formData->if_any_surgery);
        $count2 = 1;
        foreach($any_surgery as $any){
            ?>
<tr>
<td><input type="text" name="operation<?php echo $count2;?>" value="<?php echo $any['operation'];?>" readonly /></td>
<td><input type="text" name="operationdate<?php echo $count2;?>" value="<?php echo $any['opdate'];?>" readonly/></td>
</tr>
        <?php $count2++;}
} else {
    ?>
<tr>
<td><input type="text" name="operation1" /></td>
<td><input type="text" name="operationdate1" /></td>
</tr>
<tr>
<td><input type="text" name="operation2" /></td>
<td><input type="text" name="operationdate2" /></td>
</tr>
<tr>
<td><input type="text" name="operation3" /></td>
<td><input type="text" name="operationdate3" /></td>
</tr>
<?php } ?>
</tbody>
</table>
<label>Have you had any allergy to any medications?</label>
<table style="height: 5px; width: 711px;">
<tbody>
<tr>
<td>Name</td>
<td>Type of operation</td>
</tr>
<?php if(!empty($formData->if_any_allergy_to_medications)){
        $any_allergy = unserialize($formData->if_any_allergy_to_medications);
        $count3 = 1;
        foreach($any_allergy as $anyall){
            ?>
<tr>
<td><input type="text" name="allergy<?php echo $count3;?>" value="<?php echo $anyall['allergy'];?>" readonly /></td>
<td><input type="text" name="opallergy<?php echo $count3;?>" value="<?php echo $anyall['opallergy'];?>" readonly/></td>
</tr>
        <?php $count3++;}
} else {
    ?>
<tr>
<td><input type="text" name="allergy1" /></td>
<td><input type="text" name="opallergy1" /></td>
</tr>
<tr>
<td><input type="text" name="allergy2" /></td>
<td><input type="text" name="opallergy2" /></td>
</tr>
<tr>
<td></td>
<td></td>
</tr>
<?php } ?>
</tbody>
</table>
<label>Current Medications</label>
<table style="height: 5px; width: 711px;">
<tbody>
<tr>
<td>Name</td>
<td>Dose</td>
<td>Frequency</td>
</tr>
<?php if(!empty($formData->current_medications)){
        $currentmed = unserialize($formData->current_medications);
        $count4 = 1;
        foreach($currentmed as $med){
            ?>
<tr>
<td><input type="text" name="name<?php echo $count4;?>" value="<?php echo $med['name'];?>" readonly /></td>
<td><input type="text" name="dose<?php echo $count4;?>" value="<?php echo $med['dose'];?>" readonly/></td>
<td><input type="text" name="frq<?php echo $count4;?>" value="<?php echo $med['frequency'];?>" readonly/></td>
</tr>
        <?php $count4++;}
} else {
    ?>
<tr>
<td><input type="text" name="name1" /></td>
<td><input type="text" name="dose1" /></td>
<td><input type="text" name="frq1" /></td>
</tr>
<tr>
<td><input type="text" name="name2" /></td>
<td><input type="text" name="dose2" /></td>
<td><input type="text" name="frq2" /></td>
</tr>
<tr>
<td><input type="text" name="name3" /></td>
<td><input type="text" name="dose3" /></td>
<td><input type="text" name="frq3" /></td>
</tr>
<?php } ?>
</tbody>
</table>
<div style="clear: both;"></div>
<input class="saveform" type="submit" name="Save" id="saveform" value="Save" style="float:right;" >
</fieldset>


<fieldset class="sectionwrap">
<label>In the past 3 years have you had a...</label><br/>
<div style="float: left;"><input name="stool_test" type="checkbox" value="Yes" <?php if($formData->if_stool_tested_for_blood == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Stool tested for blood<br/>
<input name="colonoscopy" type="checkbox" value="Yes" <?php if($formData->if_colonoscopy == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Flexible sigmoidoscopy or colonoscopy<br/>
<input name="blood_cholesterol_level" type="checkbox" value="Yes" <?php if($formData->if_blood_cholesterol_level == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Blood cholesterol level<br/>
<input name="psa" type="checkbox" value="Yes" <?php if($formData->if_psa == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Prostatic specific antigen test (PSA)<br/>
<input name="chest_xray" type="checkbox" value="Yes" <?php if($formData->if_chest_xray == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Chest x-ray<br/></div>
<div style="float: left; padding-left: 100px;"><input name="ecg" type="checkbox" value="Yes" <?php if($formData->if_ecg == 'yes'){echo 'checked'; ?> onclick="return false;" <?php }?>/> Electrocardiogram (ECG)<br/>
<input name="mammogram" type="checkbox" value="Yes" <?php if($formData->if_mammogram == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> Mammogram<br/>
<input name="pap_pelvic" type="checkbox" value="Yes" <?php if($formData->if_pap_pelvic == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> Pap Smear and Pelvic exam<br/>
<input name="ctscan_abdomen" type="checkbox" value="Yes" <?php if($formData->if_ctscan_abdomen == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> CT scan of abdomen<br/>
<input name="liver_biopsy" type="checkbox" value="Yes" <?php if($formData->if_liver_biopsy == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> Liver biopsy<br/></div>
<div style="clear: both;"></div>
<h4>REVIEW OF SYSTEMS:</h4>
<label>Do you have or have had any of the following problems</label>
<div>
<div style="clear:both"></div>
<label><b>General:</b></label><br/>
<div style="float: left;">

<input name="poor_appetite" type="checkbox" value="Yes" <?php if($formData->if_poor_appetite == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> poor appetite<br/>
<input name="weight_loss" type="checkbox" value="Yes" <?php if($formData->if_weight_loss == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> weight loss<br/>
<input name="weight_gain" type="checkbox" value="Yes" <?php if($formData->if_weight_gain == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> weight gain<br/>
<input name="easy_fatigability" type="checkbox" value="Yes" <?php if($formData->if_easy_fatigability == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> easy fatigability<br/>
<input name="transfusion" type="checkbox" value="Yes" <?php if($formData->if_transfusion == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> transfusion<br/>

</div>
<div style="float: left; padding-left: 100px;"><input name="anxiety" type="checkbox" value="Yes" <?php if($formData->if_anxiety == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> anxiety<br/>
<input name="fever" type="checkbox" value="Yes" <?php if($formData->if_fever == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> fever or abnormal sweating<br/>
<input name="itching" type="checkbox" value="Yes" <?php if($formData->if_itching == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> itching<br/>
<input name="depression" type="checkbox" value="Yes" <?php if($formData->if_depression == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> depression<br/></div>
</div>
<div style="clear: both;"></div>
<div>

<label><b>Head:</b></label><br/>
<div style="float: left;">

<input name="eye_trouble" type="checkbox" value="Yes" <?php if($formData->if_eye_trouble == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> eye trouble<br/>
<input name="hearing_disorder" type="checkbox" value="Yes" <?php if($formData->if_hearing_disorder == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> hearing disorder<br/>

</div>
<div style="float: left; padding-left: 100px;"><input name="sore_tongue" type="checkbox" value="Yes" <?php if($formData->if_sore_tongue == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> sore tongue or mouth<br/>
<input name="yellow_eyes" type="checkbox" value="Yes" <?php if($formData->if_yellow_eyes == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> yellow eyes<br/></div>
</div>
<div style="clear: both;"></div>
<div><label><b>Neck:</b></label><br/>
<input name="goiterd" type="checkbox" value="Yes" <?php if($formData->if_goiter == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> goiter<br/>
<input name="lumps" type="checkbox" value="Yes " <?php if($formData->if_lumps == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> lumps or masses<br/></div>
<div>

<label><b>Chest:</b></label><br/>
<div style="float: left;">

<input name="chest_pain" type="checkbox" value="Yes" <?php if($formData->if_chest_pain == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> chest pain<br/>
<input name="breathd" type="checkbox" value="Yes" <?php if($formData->if_shortnessof_breath == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> shortness of breath<br/>
<input name="palpitations" type="checkbox" value="Yes" <?php if($formData->if_palpitations == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/>palpitations

</div>
<div style="float: left; padding-left: 100px;"><input name="asthma" type="checkbox" value="Yes" <?php if($formData->if_asthma == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> Asthma<br/>
<input name="chronic_cough" type="checkbox" value="Yes" <?php if($formData->if_chronic_cough == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> chronic cough<br/>
<input name="high_bp" type="checkbox" value="Yes" <?php if($formData->if_high_bp == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> high blood pressure<br/></div>
</div>
<div style="clear: both;"></div>
<div>

<label><b>GI:</b></label><br/>
<div style="float: left;"><input name="heart_burn" type="checkbox" value="Yes" <?php if($formData->if_heart_burn == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> heart burn<br/>
<input name="difficulty_swallowing" type="checkbox" value="Yes" <?php if($formData->if_difficulty_swallowing == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> difficulty swallowing<br/>
<input name="indigestiond" type="checkbox" value="Yes" <?php if($formData->if_indigestion == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> indigestion<br/>
<input name="milk_intoleranced" type="checkbox" value="yes" <?php if($formData->if_milk_intolerance == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> milk intolerance<br/>
<input name="persistent_nausea" type="checkbox" value="Yes " <?php if($formData->if_persistent_nausea == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> persistent nausea or vomiting<br/>
<input name="vomiting_blood_gi" type="checkbox" value="yes" <?php if($formData->if_vomiting_blood == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> vomiting blood<br/></div>
<div style="float: left; padding-left: 100px;"><input name="passing_blood" type="checkbox" value="Yes" <?php if($formData->if_passing_blood == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> passing blood<br/>
<input name="abdominal_pain" type="checkbox" value="Yes" <?php if($formData->if_abdominal_pain == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> abdominal pain<br/>
<input name="diarrhea" type="checkbox" value="Yes" <?php if($formData->if_diarrhea == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> diarrhea<br/>
<input name="constipation_gi" type="checkbox" value="yes" <?php if($formData->if_constipation == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> constipation<br/>
<input name="abdominal_swelling_gi" type="checkbox" value="Yes" <?php if($formData->if_abdominal_swelling == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> abdominal swelling<br/></div>
</div>
<div style="clear: both;"></div>
<div>

<label><b>GU:</b></label><br/>
<div style="float: left;"><input name="urination" type="checkbox" value="Yes" <?php if($formData->if_difficulty_with_urination == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> difficulty with urination<br/>
<input name="blood_in_urine" type="checkbox" value="Yes" <?php if($formData->if_blood_in_urine == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> blood in urine<br/></div>
<div style="float: left; padding-left: 100px;"><input name="dark_urine" type="checkbox" value="Yes" <?php if($formData->if_dark_urine == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> dark urine<br/>
<input name="kidney_stones" type="checkbox" value="Yes" <?php if($formData->if_kidney_stones == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> kidney stones<br/></div>
</div>
<div style="clear: both;"></div>
<div>
<label><b>Only Females:</b></label><br/>
<label>Any difficulty with menstrual periods? </label>
<?php
    if(!empty($formData->if_difficulty_with_MP)){
?>
<input class="info-value" name="difficulty_with_MP" type="text" style="width:45px !important" value="<?php echo $formData->if_difficulty_with_MP; ?>" readonly>
    <?php } else { ?>
<select name="difficulty_with_MP">
    <option value="yes">Yes</option>
    <option value="no">No</option>
</select>
    <?php } ?>
<br/>
<input name="last_MP" type="checkbox" value="yes" <?php if($formData->last_menstrual_period == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> Last menstrual period<br/>
<input name="contraceptive_use" type="checkbox" value="yes" <?php if($formData->if_contraceptive_use == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> Contraceptive use<br/>
<input name="estrogen" type="checkbox" value="yes" <?php if($formData->if_estrogen_replacement == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> Estrogen replacement?<br/>
</div>
<div>
<label><b>Extremities:</b></label><br/>
<div style="float: left;">
<input name="arthritisd" type="checkbox" value="Yes" <?php if($formData->if_arthritis == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> arthritis<br/>
<input name="swollen_legs" type="checkbox" value="Yes" <?php if($formData->if_swollen_legs == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> swollen legs<br/>
<input name="cold_sensitivity" type="checkbox" value="Yes" <?php if($formData->if_cold_sensitivity == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> cold sensitivity<br/>
</div></div>
<div style="clear: both;"></div>
<div>

<label><b>Neurologic:</b></label><br/>
<div style="float: left;">
<input name="recurrent_headaches" type="checkbox" value="Yes" <?php if($formData->if_recurrent_headaches == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> recurrent headaches<br/>
<input name="consciousness" type="checkbox" value="Yes" <?php if($formData->if_loss_of_consciousness == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> loss of consciousness<br/>
<input name="seizures" type="checkbox" value="Yes" <?php if($formData->if_seizures == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> seizures<br/>
<input name="loss_of_memory" type="checkbox" value="Yes" <?php if($formData->if_loss_of_memory == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> loss of memory<br/>
</div>
<div style="float: left; padding-left: 100px;">
<input name="confusion" type="checkbox" value="Yes" <?php if($formData->if_confusion == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> confusion<br/>
<input name="tremor" type="checkbox" value="Yes" <?php if($formData->if_tremor == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> tremor<br/>
<input name="weakness" type="checkbox" value="Yes" <?php if($formData->if_weakness == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> weakness or numbness of face or extremities<br/>
</div>
</div>
<div style="clear: both;"></div>
<label class="info-label" style="padding-top:32px;"> Current Weight </label>
<input class="info-value" type="text" name="current_weight" <?php if(!empty($formData->current_weight)){echo "Value=".$formData->current_weight;}?> <?php if(!empty($formData->current_weight)){echo "readonly";} ?>>
<label class="info-label" style="padding-top:32px;"> Lowest Weight </label>
<input class="info-value" type="text" name="lowest_weight" <?php if(!empty($formData->lowest_weight)){echo "Value=".$formData->lowest_weight;}?> <?php if(!empty($formData->lowest_weight)){echo "readonly";} ?>>
<label class="info-label" style="padding-top:32px;"> Highest Weight </label>
<input class="info-value" type="text" name="highest_weight" <?php if(!empty($formData->highest_weight)){echo "Value=".$formData->highest_weight;}?> <?php if(!empty($formData->highest_weight)){echo "readonly";} ?>>
<div style="clear: both;"></div>
<div>

<label><b>IMMUNIZATIONS:</b></label><br/>
<div style="float: left;">
<input name="influenza" type="checkbox" value="Yes" <?php if($formData->if_influenza == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> Influenza<br/>
<input name="tetanus" type="checkbox" value="Yes" <?php if($formData->if_tetanus == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> Tetanus/diphtheria<br/>
</div>
<div style="float: left; padding-left: 100px;">
<input name="pneumonia" type="checkbox" value="Yes" <?php if($formData->if_pneumonia == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> pneumonia<br/>
<input name="hepatitisB" type="checkbox" value="Yes" <?php if($formData->if_hepatitisB == 'yes'){echo 'checked '; ?> onclick="return false;" <?php }?>/> hepatitis B<br/>
</div>
</div>
<div style="clear: both;"></div>
<input class="saveform" type="submit" name="Save" id="saveform" value="Save" style="float:right;" >
</fieldset>
<fieldset class="sectionwrap">
<label><strong>Education</strong></label><br/>
<label class="info-label">How many years of school have you completed?</label> <input class="info-value" type="text" name="years_of_education" <?php if(!empty($formData->years_of_education)){echo "Value=".$formData->years_of_education;}?> <?php if(!empty($formData->years_of_education)){echo "readonly";} ?>/><br/>
<label><strong>Occupation</strong></label><br/>
<label class="info-label">Current employment status:</label> <input <?php if( $formData->current_employment_status ) { if($formData->current_employment_status === 'employed'){echo "checked";} else{?> disabled="disabled" <?php } } ?> type="radio" name="current_employment_status" value="employed" />Employed <input  type="radio" name="occupation" value="retired" <?php  if( $formData->current_employment_status ) { if($formData->current_employment_status === 'retired'){echo "checked";} else{?> disabled="disabled" <?php } }?>/>Retired <input type="radio" name="occupation" value="unemployed" <?php  if( $formData->current_employment_status ) { if($formData->current_employment_status === 'unemployed'){echo "checked";} else{?> disabled="disabled" <?php } } ?>/>Unemployed <input type="radio" name="occupation" value="homemaker" <?php  if( $formData->current_employment_status ) { if($formData->current_employment_status === 'homemaker'){echo "checked";} else{?> disabled="disabled" <?php } }?>/>Homemaker 
<div style="clear:both;"></div>
<?php if($formData->current_employment_status === 'employed') { ?>
<label> If Employed -> </label><br/>
<label class="info-label">Current occupation </label> <input type="text" name="current_occupation" class="info-value" <?php if(!empty($formData->current_occupation)){echo "Value=".$formData->current_occupation;}?> <?php if(!empty($formData->current_occupation)){echo "readonly";} ?>><br/>
<label class="info-label">Previous occupation </label> <input type="text" name="previous_occupation" class="info-value" <?php if(!empty($formData->current_occupation)){echo "Value=".$formData->current_occupation;}?> <?php if(!empty($formData->current_occupation)){echo "readonly";} ?>><br/>
<?php } ?>
<label><strong>Disability</strong></label><br/>
<div>
<label class="info-label">Are you disabled?</label>
<input type="radio" name="disabled" value="Yes" <?php if($formData->if_disabled) {if($formData->if_disabled === 'yes'){echo "checked";} else{?> disabled="disabled" <?php }} ?>/>YES  <input type="radio" name="disabled" value="No" <?php if($formData->if_disabled) { if($formData->if_disabled === 'no'){echo "checked";} else{?> disabled="disabled" <?php }}?> />NO
</div>
<div style="clear:both;"></div>
<div class="cause">
<label class="info-label"> Cause</label> <input class="info-value" name="disabled_cause" type="text" <?php if(!empty($formData->disabled_cause)){echo "Value=".$formData->disabled_cause;}?> <?php if(!empty($formData->disabled_cause)){echo "readonly";} ?>> 
</div>
<div>
<label><strong>Abuse</strong> </label><br/>
<label class="info-label">Have you ever been physically, sexually or emotionally abused ? </label>
<input type="radio" name="abused" value="Yes" <?php if($formData->if_abused){ if($formData->if_abused === 'yes'){echo "checked";} else{?> disabled="disabled" <?php }} ?>>YES <input type="radio" name="abused" value="No" <?php if($formData->if_abused){ if($formData->if_abused === 'no'){echo "checked";} else{?> disabled="disabled" <?php } }?>>NO
</div>
<div style="clear:both;"></div>
<div>
<label>Have you used any of the following substances? </label>
<table>
<tr>
<th> Substance</th>
<th>Current Use</th>
<th>Previous Use</th>
<th>Type/Amount</th>
<th>Length/If stopped when</th>
</tr>
<?php $usedSub = unserialize($formData->used_substances); ?>
<tr>
<td>Caffeine (coffee, tea, soda)</td>
<td><input type="text" name="caffcurrent" value="<?php if(!empty($usedSub)){echo $usedSub['caffcurrent'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
<td><input type="text" name="caffprevious" value="<?php if(!empty($usedSub)){echo $usedSub['caffprevious'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
<td><input type="text" name="cafftype" value="<?php if(!empty($usedSub)){echo $usedSub['cafftype'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
<td><input type="text" name="cafflength" value="<?php if(!empty($usedSub)){echo $usedSub['cafflength'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
</tr>
<tr>
<td>Tobacco</td>
<td><input type="text" name="tobcurrent" value="<?php if(!empty($usedSub)){echo $usedSub['tobcurrent'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
<td><input type="text" name="tobprevious" value="<?php if(!empty($usedSub)){echo $usedSub['tobprevious'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
<td><input type="text" name="tobtype" value="<?php if(!empty($usedSub)){echo $usedSub['tobtype'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
<td><input type="text" name="toblength" value="<?php if(!empty($usedSub)){echo $usedSub['toblength'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
</tr>
<tr>
<td>Alcohol (beer, wine, liquor)</td>
<td><input type="text" name="alcurrent" value="<?php if(!empty($usedSub)){echo $usedSub['alcurrent'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
<td><input type="text" name="alprevious" value="<?php if(!empty($usedSub)){echo $usedSub['alprevious'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
<td><input type="text" name="altype" value="<?php if(!empty($usedSub)){echo $usedSub['altype'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
<td><input type="text" name="allength" value="<?php if(!empty($usedSub)){echo $usedSub['allength'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
</tr>
<tr>
<td>Recreational/street drugs</td>
<td><input type="text" name="drcurrent" value="<?php if(!empty($usedSub)){echo $usedSub['drcurrent'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
<td><input type="text" name="drprevious" value="<?php if(!empty($usedSub)){echo $usedSub['drprevious'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
<td><input type="text" name="drtype" value="<?php if(!empty($usedSub)){echo $usedSub['drtype'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
<td><input type="text" name="drlength" value="<?php if(!empty($usedSub)){echo $usedSub['drlength'];}?>" <?php if(!empty($usedSub)){echo 'readonly';}?>/></td>
</tr>
</table>
</div>
<div style="clear:both"></div>
<div>
<label class="info-label">Marital Status:</label> <input <?php if($formData->marital_status) { if($formData->marital_status === 'single'){echo "checked";} else{?> disabled="disabled" <?php }} ?> type="radio" name="marital_status" value="single"/>Single <input type="radio" name="marital_status" value="married" <?php if($formData->marital_status) { if($formData->marital_status === 'married'){echo "checked";} else{?> disabled="disabled" <?php } }?>/>Married <input type="radio" name="marital_status" value="separated" <?php if($formData->marital_status) { if($formData->marital_status === 'separated'){echo "checked";} else{?> disabled="disabled" <?php }} ?>>Separated <input type="radio" name="marital_status" value="divorced" <?php if($formData->marital_status) { if($formData->marital_status === 'divorced'){echo "checked";} else{?> disabled="disabled" <?php } }?>>Divorced
</div>
<div style="clear:both"></div>
<div>
<label class="info-label">Current Spouse:</label> <input  type="radio" name="current_spousee" value="na" <?php if($formData->current_spouse) { if($formData->current_spouse === 'na'){echo "checked";}  else{?> disabled="disabled" <?php } }?>/>NA <input type="radio" name="current_spousee" value="alive" <?php if($formData->current_spouse) { if($formData->current_spouse === 'alive'){echo "checked";}  else{?> disabled="disabled" <?php } }?>/>Alive <input type="radio" name="current_spousee" value="health" <?php if($formData->current_spouse) { if($formData->current_spouse === 'health'){echo "checked";}  else{?> disabled="disabled" <?php } } ?>>Health problems <input type="radio" name="current_spousee" value="death" <?php if($formData->current_spouse){ if($formData->current_spouse === 'death'){echo "checked";}  else{?> disabled="disabled" <?php }} ?>>Cause of death
</div>
<div style="clear:both"></div>
<div>
<label class="info-label">Current employment status:</label> <input type="radio" name="spouse_employment_status" value="employed" <?php if($formData->spouse_employment_status) { if($formData->spouse_employment_status === 'employed'){echo "checked";}  else{?> disabled="disabled" <?php } } ?>/>Employed <input  type="radio" name="spouse_employment_status" value="retired" <?php if($formData->spouse_employment_status){ if($formData->spouse_employment_status === 'retired'){echo "checked";}  else{?> disabled="disabled" <?php }} ?>/>Retired <input type="radio" name="spouse_employment_status" value="unemployed" <?php if($formData->spouse_employment_status){ if($formData->spouse_employment_status === 'unemployed'){echo "checked";}  else{?> disabled="disabled" <?php }} ?>/>Unemployed <input type="radio" name="spouse_employment_status" value="homemaker" <?php if($formData->spouse_employment_status){ if($formData->spouse_employment_status === 'homemaker'){echo "checked";}  else{?> disabled="disabled" <?php } } ?>/>Homemaker
</div> 
<div style="clear:both;"></div>
<label> If Employed -> </label><br/>
<label class="info-label">Current occupation </label> <input type="text" name="spouse_current_occupation" class="info-value" value="<?php if(!empty($formData->spouse_current_occupation)){echo $formData->spouse_current_occupation;}?>" <?php if(!empty($formData->spouse_current_occupation)){echo 'readonly';}?>><br/>
<label class="info-label"> Do you exercise regularly?</label> Type/Frequency<input class ="info-value" type="text" name="exercise" value="<?php if(!empty($formData->do_you_excercise)){echo $formData->do_you_excercise;}?>" <?php if(!empty($formData->do_you_excercise)){echo 'readonly';}?>/><br/>
<div><div style="clear:both;"></div>
<label><strong>Sleep: </strong></label><br/>
<label class="info-label">Do you have difficulty falling asleep?</label> <input type="radio" name="sleep" value="no" <?php if($formData->if_difficulty_asleep) { if($formData->if_difficulty_asleep === 'no'){echo "checked";}  else{?> disabled="disabled" <?php } } ?> />NO <input  type="radio" name="sleep" value="yes" <?php if($formData->if_difficulty_asleep) { if($formData->if_difficulty_asleep === 'yes'){echo "checked";}  else{?> disabled="disabled" <?php } } ?>/>Yes
</div>
<div style="clear:both;"></div>
<div>
<label class="info-label">Do you awaken early in the morning without apparent cause?</label><input type="radio" name="awaken_early" value="no" <?php if($formData->if_awaken_early) { if($formData->if_awaken_early === 'no'){echo "checked"; }  else{?> disabled="disabled" <?php } } ?>/>NO <input   type="radio" name="awaken_early" value="yes" <?php if($formData->if_awaken_early) { if($formData->if_awaken_early === 'yes'){echo "checked";}  else{?> disabled="disabled" <?php } }?>/>Yes

</div>
<input class="saveform" type="submit" name="Save" id="saveform" value="Save" style="float:right;" >
</fieldset>
<fieldset class="sectionwrap">
<label>Family History If living If Deceased</label>
<table>
<tr>
<th>Relation</th>
<th>Age</th>
<th>Health</th>
<th>Age of Death</th>
<th>Cause</th>
</tr>
<?php $familyHistory = unserialize($formData->family_history); ?>
<tr>
<td>Father</td>
<td><input type="text" name="fage" value="<?php if(!empty($familyHistory)){echo $familyHistory['fage'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
<td><input type="text" name="fhealth" value="<?php if(!empty($familyHistory)){echo $familyHistory['fhealth'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
<td><input type="text" name="faged" value="<?php if(!empty($familyHistory)){echo $familyHistory['faged'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
<td><input type="text" name="fcause" value="<?php if(!empty($familyHistory)){echo $familyHistory['fcause'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
</tr>
<tr>
<td>Mother</td>
<td><input type="text" name="mage" value="<?php if(!empty($familyHistory)){echo $familyHistory['mage'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
<td><input type="text" name="mhealth" value="<?php if(!empty($familyHistory)){echo $familyHistory['mhealth'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
<td><input type="text" name="maged" value="<?php if(!empty($familyHistory)){echo $familyHistory['maged'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
<td><input type="text" name="mcause" value="<?php if(!empty($familyHistory)){echo $familyHistory['mcause'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
</tr>
<tr>
<td>
<input type="radio" name="sibling" value="brother" <?php if($familyHistory['sibling'] === 'brother'){echo "checked";}?> >Brother
<input type="radio" name="sibling" value="sister" <?php if($familyHistory['sibling'] === 'sister'){echo "checked";}?>>Sister
</td>
<td><input type="text" name="sage" value="<?php if(!empty($familyHistory)){echo $familyHistory['sage'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
<td><input type="text" name="shealth" value="<?php if(!empty($familyHistory)){echo $familyHistory['shealth'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
<td><input type="text" name="saged" value="<?php if(!empty($familyHistory)){echo $familyHistory['saged'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
<td><input type="text" name="scause" value="<?php if(!empty($familyHistory)){echo $familyHistory['scause'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
</tr>
<tr>
<td>
<input type="radio" name="husband" value="husband" <?php if($familyHistory['husband'] === 'husband'){echo "checked";}?>>Husband
<input type="radio" name="husband" value="wife" <?php if($familyHistory['husband'] === 'wife'){echo "checked";}?>>Wife
</td>
<td><input type="text" name="hage" value="<?php if(!empty($familyHistory)){echo $familyHistory['hage'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
<td><input type="text" name="hhealth" value="<?php if(!empty($familyHistory)){echo $familyHistory['hhealth'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
<td><input type="text" name="haged" value="<?php if(!empty($familyHistory)){echo $familyHistory['haged'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
<td><input type="text" name="hcause" value="<?php if(!empty($familyHistory)){echo $familyHistory['hcause'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
</tr>
<tr>
<td>
<input type="radio" name="son" value="son" <?php if($familyHistory['son'] === 'son'){echo "checked";}?>>Son
<input type="radio" name="son" value="daughter" <?php if($familyHistory['son'] === 'daughter'){echo "checked";}?>>Daughter
</td>
<td><input type="text" name="soage" value="<?php if(!empty($familyHistory)){echo $familyHistory['soage'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
<td><input type="text" name="sohealth" value="<?php if(!empty($familyHistory)){echo $familyHistory['sohealth'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
<td><input type="text" name="soaged" value="<?php if(!empty($familyHistory)){echo $familyHistory['soaged'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
<td><input type="text" name="socause" value="<?php if(!empty($familyHistory)){echo $familyHistory['socause'];}?>" <?php if(!empty($familyHistory)){echo 'readonly';}?>/></td>
</tr>
</table>
<div>
<label>Do you know of any blood relative who has or had:</label>
<?php $relative = unserialize($formData->blood_relative);?>
<div>
<input type="checkbox" name="" <?php if(!empty($relative['cancer'])){echo 'checked '; ?> onclick="return false;" <?php }?>>Cancer <input class="info-value" type="text" name="cancer" value="<?php if(!empty($relative['cancer'])){echo $relative['cancer'];}?>" <?php if(!empty($relative['cancer'])){echo 'readonly';}?>></div><div style="clear:both"<</div>
<div>
<input type="checkbox" name="" <?php if(!empty($relative['breast'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Breast<input class="info-value" type="text" name="breast" value="<?php if(!empty($relative['breast'])){echo $relative['breast'];}?>" <?php if(!empty($relative['breast'])){echo 'readonly';}?>>
</div><div style="clear:both"</div>
<div>
<input type="checkbox" name="" <?php if(!empty($relative['colon'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Colon<input class="info-value" type="text" name="colon" value="<?php if(!empty($relative['colon'])){echo $relative['colon'];}?>" <?php if(!empty($relative['colon'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name="" <?php if(!empty($relative['ovary'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Ovary<input class="info-value" type="text" name="ovary" value="<?php if(!empty($relative['ovary'])){echo $relative['ovary'];}?>" <?php if(!empty($relative['ovary'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name="" <?php if(!empty($relative['uterus'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Uterus<input class="info-value" type="text" name="uterus" value="<?php if(!empty($relative['uterus'])){echo $relative['uterus'];}?>" <?php if(!empty($relative['uterus'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name="" <?php if(!empty($relative['stroke'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Stroke/TIA<input class="info-value" type="text" name="stroke" value="<?php if(!empty($relative['stroke'])){echo $relative['stroke'];}?>" <?php if(!empty($relative['stroke'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name="" <?php if(!empty($relative['HBP'])){echo 'checked '; ?> onclick="return false;" <?php }?>> High blood pressure<input class="info-value" type="text" name="HBP" value="<?php if(!empty($relative['HBP'])){echo $relative['HBP'];}?>" <?php if(!empty($relative['HBP'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name="" <?php if(!empty($relative['triglycerides'])){echo 'checked '; ?> onclick="return false;" <?php }?>> High cholesterol/ triglycerides<input class="info-value" type="text" name="triglycerides" value="<?php if(!empty($relative['triglycerides'])){echo $relative['triglycerides'];}?>" <?php if(!empty($relative['triglycerides'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name=" " <?php if(!empty($relative['heart'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Heart disease <input class="info-value" type="text" name="heart" value="<?php if(!empty($relative['heart'])){echo $relative['heart'];}?>" <?php if(!empty($relative['heart'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name="" <?php if(!empty($relative['diabetes'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Diabetes<input class="info-value" type="text" name="diabetes" value="<?php if(!empty($relative['diabetes'])){echo $relative['diabetes'];}?>" <?php if(!empty($relative['diabetes'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name="" <?php if(!empty($relative['ulcer'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Ulcer (duodenal)<input class="info-value" type="text" name="ulcer" value="<?php if(!empty($relative['ulcer'])){echo $relative['ulcer'];}?>" <?php if(!empty($relative['ulcer'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name="" <?php if(!empty($relative['liver'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Liver disease<input class="info-value" type="text" name="liver" value="<?php if(!empty($relative['liver'])){echo $relative['liver'];}?>" <?php if(!empty($relative['liver'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name=" " <?php if(!empty($relative['kidney'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Kidney disease<input class="info-value" type="text" name="kidney " value="<?php if(!empty($relative['kidney'])){echo $relative['kidney'];}?>" <?php if(!empty($relative['kidney'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name=" " <?php if(!empty($relative['lung'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Lung disease<input class="info-value" type="text" name="lung" value="<?php if(!empty($relative['lung'])){echo $relative['lung'];}?>" <?php if(!empty($relative['lung'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name=" " <?php if(!empty($relative['genetic'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Genetic disorder<input class="info-value" type="text" name="genetic " value="<?php if(!empty($relative['genetic'])){echo $relative['genetic'];}?>" <?php if(!empty($relative['genetic'])){echo 'readonly';}?>></div>
<div style="clear:both"</div>
<label>Other issues if known</label>
<div>
<input type="checkbox" name="" <?php if(!empty($relative['cancer'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Epilepsy<input class="info-value" type="text" name="epilepsy" value="<?php if(!empty($relative['epilepsy'])){echo $relative['epilepsy'];}?>" <?php if(!empty($relative['epilepsy'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name="" <?php if(!empty($relative['migraine'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Migraine<input class="info-value" type="text" name="migraine" value="<?php if(!empty($relative['migraine'])){echo $relative['migraine'];}?>" <?php if(!empty($relative['migraine'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name=" " <?php if(!empty($relative['mental'])){echo 'checked '; ?> onclick="return false;" <?php }?>>Mental illness<input class="info-value" type="text" name="mental" value="<?php if(!empty($relative['mental'])){echo $relative['mental'];}?>" <?php if(!empty($relative['mental'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name=" " <?php if(!empty($relative['alcohol'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Alcohol or drug abuse<input class="info-value" type="text" name="alcohol " value="<?php if(!empty($relative['alcohol'])){echo $relative['alcohol'];}?>" <?php if(!empty($relative['alcohol'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name=" " <?php if(!empty($relative['goiter'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Goiter or gastric<input class="info-value" type="text" name="goiter" value="<?php if(!empty($relative['goiter'])){echo $relative['goiter'];}?>" <?php if(!empty($relative['goiter'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name="" <?php if(!empty($relative['arthritis'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Arthritis<input class="info-value" type="text" name="arthritis" value="<?php if(!empty($relative['arthritis'])){echo $relative['arthritis'];}?>" <?php if(!empty($relative['arthritis'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name=""<?php if(!empty($relative['crohn'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Crohn's disease<input class="info-value" type="text" name="crohn" value="<?php if(!empty($relative['crohn'])){echo $relative['crohn'];}?>" <?php if(!empty($relative['crohn'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<div>
<input type="checkbox" name=" " <?php if(!empty($relative['bowel'])){echo 'checked '; ?> onclick="return false;" <?php }?>> Irritable bowel syndrome<input class="info-value" type="text" name="bowel " value="<?php if(!empty($relative['bowel'])){echo $relative['bowel'];}?>" <?php if(!empty($relative['bowel'])){echo 'readonly';}?>></div><div style="clear:both"</div>
<input class="saveform" type="submit" name="Save" id="saveform" value="Save" style="float:right;" >

<input type="submit" value="Submit" class="saveform" style="float:right;margin-right:10px;" />

<input type="button" name="next" value="Next" class="next" onclick="window.location='http://uat-americandoc.bintg.damcogroup.com/materials-checklist/';" style="float:right;margin-right:15px;">

</fieldset>
<div id="loadingmessage" style="display:none">
  <img src="<?php bloginfo('template_url') ?>/images/loader.gif" class="ajax-loader"/>
</div>
<!--<fieldset class="sectionwrap">
<h3>Materials Checklist</h3>
<h4>Please include all documents that can be obtained</h4>
<ul>
	<li>MRI, CT or X-Ray Films (as appropriate)</li>
	<li>Office Visit Notes</li>
	<li>Laboratory Results (as appropriate)</li>
	<li>Pertinent Surgical Reports</li>
	<li>Hospital Discharge Summaries</li>
	<li>Current Medications and Dosage Information (also indicate on medical history
questionnaire)</li>
</ul>
If you have questions, please email <a href="mailto:support@americandoc.com">support@americandoc.com</a>
<?php //echo do_shortcode('[wordpress_file_upload]'); ?>
</fieldset>
<fieldset class="sectionwrap">
  <table>
        <tr>
        <th> </th>
        <th> Type of Consult</th>
        <th> Pricing </th>
        </tr>
        <tr>
            <td><input type="checkbox" name="single_specialty" value="10000" class="pricing"/></td>
            <td>Single Specialty Consults</td>
            <td>10000 Rs</td>      
        </tr>
        <tr>
            <td><input type="checkbox" name="multi_specialty" value="15000" class="pricing"/></td>
            <td>Multi Specialty Consults</td>
            <td>15000 Rs</td>  
        </tr>
         <tr>
            <td><input type="checkbox" name="orthopedic_cardiac" value="30000" class="pricing"/></td>
            <td>Orthopedic/Cardiac Surgeon Consult</td>
            <td>30000 Rs</td>  
        </tr>
        
         <tr>
            <td><input type="checkbox" name="diabetes_consult" value="12500" class="pricing"/></td>
            <td>Diabetes Consult</td>
            <td>12500 Rs</td>  
        </tr>
         <tr>
            <td><input type="checkbox" name="Mirrored_care_service" value="40000" class="pricing"/></td>
            <td>Mirrored Care Service</td>
            <td>40000+/month Rs</td>  
        </tr>
         <tr>
            <td><input type="checkbox" name="nutrition_consult" value="4000" class="pricing"/></td>
            <td>Nutrition Consult</td>
            <td>4000 Rs</td>  
        </tr>
         <tr>
            <td><input type="checkbox" name="speedy_service" value="2000" class="pricing"/></td>
            <td>Speedy Service (receive consult within 24 hours)</td>
            <td>2000 Rs(extra)</td>  
        </tr>
         <tr>
             <td></td>
            <td>Total</td>
            <td><input type="text" name="total" style="border:0px;" class="total_amount" value="0"></td>  
        </tr>
		
    </table>
<input type="submit" value="Submit" class="saveform" style="float:right;" name="price_submit" />

</fieldset>
<fieldset>
<?php //echo $id = 161; $p = get_page($id); echo apply_filters('the_content', $p->post_content); ?>
</fieldset>
<fieldset class="sectionwrap">


<?php //get_header(); ?>
<div id="container" style="width:50%;margin-left:100px;">
<form action="http://uat-americandoc.bintg.damcogroup.com/posttozaakpay" method="post">
Your Email: <input name="buyerEmail" type="text" value="" />
Your First Name: <input name="buyerFirstName" type="text" value="" />
Your Last Name: <input name="buyerLastName" type="text" value="" />
Your Address : <input name="buyerAddress" type="text" value="" />
Your City: <input name="buyerCity" type="text" value="" />
Your State: <input name="buyerState" type="text" value="" />
Your Country: <input align="middle" name="buyerCountry" type="text" value="" />
Pin Code: <input align="middle" name="buyerPincode" type="text" value="" />
Your Phone No: <input align="middle" name="buyerPhoneNumber" type="text" value="" />
Amount: <input align="middle" id="amount" name="amount" type="text" value="" />
<input align="middle" onclick="submitForm();" type="submit" value="Pay Now" /> 
<input name="txnType" type="hidden" value="1" />
<input name="zpPayOption" type="hidden" value="1" /> 
<input name="mode" type="hidden" value="1" /> 
<input name="currency" type="hidden" value="INR" /> 
<input name="merchantIpAddress" type="hidden" value="172.29.15.89" /> 
<input name="purpose" type="hidden" value="1" /> 
<input name="productDescription" type="hidden" value="Donation" /> 
<input id="txnDate" name="txnDate" type="hidden" /> 
<input name="merchantIdentifier" type="hidden" value="90468ffc336e465880597d32c8dafbb2" /> 
<input id="orderId" name="orderId" type="hidden" /> 
<input name="returnUrl" type="hidden" value="http://localhost/americandoc/zaakpayresponse" />
</form>
<script type="text/javascript">
	document.getElementById("orderId").value= "ZPLive" + String(new Date().getTime());	//	Autopopulating orderId
	var today = new Date();
	var dateString = String(today.getFullYear()).concat("-").concat(String(today.getMonth()+1)).concat("-").concat(String(today.getDate()));
	document.getElementById("txnDate").value= dateString;
</script>
<script type="text/javascript">
function submitForm()
{
    value = parseFloat(document.getElementById("amount").value);
    newvalue = parseInt(value*100);
    document.getElementById("amount").value=newvalue;
    return true;
}
</script>
</div>
<?php //get_sidebar(); ?>

<?php// get_footer(); ?>
</fieldset>-->

</form>
                
            </div><!-- #primary -->
            
            <?php if($default_page_layout == 'left_sidebar' || $default_page_layout == 'both_sidebar') : ?>
                <?php get_sidebar('left'); ?>
            <?php endif; ?>
            
        <?php if($default_page_layout == 'both_sidebar') : ?>
            </div> <!-- #primary-wrap -->
        <?php endif; ?>
        
        <?php if($default_page_layout == 'right_sidebar' || $default_page_layout == 'both_sidebar') : ?>
            <?php get_sidebar('right'); ?>
        <?php endif; ?>
    </div>
	</main><!-- #main -->
<?php endwhile; // end of the loop. ?>  	
<?php get_footer(); ?>

