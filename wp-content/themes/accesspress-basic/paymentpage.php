<?php
/*
Template Name: Zaakpay Payment Page
*/
?>
<?php
$user = get_current_user_id();
global $wpdb;
$userForm = $wpdb->get_row("SELECT * FROM wp_pricing WHERE user_id='".$user."'"); 
 
global $apbasic_options;
$apbasic_settings = get_option('apbasic_options', $apbasic_options);
if ( is_array( $apbasic_settings ) && ! empty( $apbasic_settings )) {
    extract($apbasic_settings);
}
get_header(); 


?>

<?php 
    $single_default_page_layout = get_post_meta( $post->ID, 'apbasic_page_layout', true);
    $default_page_layout = ($single_default_page_layout == 'default_layout') ? $default_page_layout : $single_default_page_layout;
    
    // Dynamically Generating Classes for #primary on the basis of page layout
    $content_class = '';
    switch($default_page_layout){
        case 'left_sidebar':
            $content_class = 'left-sidebar';
            break;
        case 'right_sidebar':
            $content_class = 'right-sidebar';
            break;
        case 'both_sidebar':
            $content_class = 'both-sidebar';
            break;
        case 'no_sidebar_wide':
            $content_class = 'no-sidebar-wide';
            break;
        case 'no_sidebar_narrow':
            $content_class = 'no-sidebar-narraow';
            break;
    }
?>
<?php while ( have_posts() ) : the_post(); ?>
	<main id="main" class="site-main <?php echo $content_class; ?>" role="main">
        <div class="ap-container">
        <?php if($default_page_layout == 'both_sidebar') : ?>
            <div id="primary-wrap" class="clearfix">
        <?php endif; ?> 
        
            <div id="primary" class="content-area">
				<?php //echo $userForm->Total;
						//echo $_POST['amount']; die();?> 	
				<form action="http://uat-americandoc.bintg.damcogroup.com/posttozaakpay" method="post">
				All fields are mandatory
				<div style="clear:both"></div>
<label class="info-label">Your Email:</label> <input  class="info-value" name="buyerEmail" type="text" value="" />
<label class="info-label">Your First Name: </label><input name="buyerFirstName" type="text" value=""  class="info-value" />
<label class="info-label">Your Last Name:</label> <input name="buyerLastName" type="text" value=""  class="info-value" />
<label class="info-label">Your Address : </label><input name="buyerAddress" type="text" value=""  class="info-value" />
<label class="info-label">Your City:</label> <input name="buyerCity" type="text" value=""  class="info-value" />
<label class="info-label">Your State: </label><input name="buyerState" type="text" value=""  class="info-value" />
<label class="info-label">Your Country: </label><input align="middle" name="buyerCountry" type="text" value=""  class="info-value" />
<label class="info-label">Pin Code:</label> <input align="middle" name="buyerPincode" type="text" value=""  class="info-value" />
<label class="info-label">Your Phone No: </label><input align="middle" name="buyerPhoneNumber" type="text" value=""  class="info-value" />
<label class="info-label">Amount: </label><input align="middle" id="amount" name="amount" type="text" value="<?php echo $userForm->Total;?>"  class="info-value" />
<div style="clear:both"></div>
<input align="middle" onclick="return submitForm();" type="submit" value="Pay Now" /> 
<input name="txnType" type="hidden" value="1" />
<input name="zpPayOption" type="hidden" value="1" /> 
<input name="mode" type="hidden" value="1" /> 
<input name="currency" type="hidden" value="INR" /> 
<input name="merchantIpAddress" type="hidden" value="103.252.245.6" /> 
<input name="purpose" type="hidden" value="1" /> 
<input name="productDescription" type="hidden" value="Donation" /> 
<input id="txnDate" name="txnDate" type="hidden" /> 
<input name="merchantIdentifier" type="hidden" value="90468ffc336e465880597d32c8dafbb2" /> 
<input id="orderId" name="orderId" type="hidden" /> 
<input name="returnUrl" type="hidden" value="http://uat-americandoc.bintg.damcogroup.com/zaakpayresponse" />
</form>
<script type="text/javascript">
	document.getElementById("orderId").value= "ZPLive" + String(new Date().getTime());	//	Autopopulating orderId
	var today = new Date();
	var dateString = String(today.getFullYear()).concat("-").concat(String(today.getMonth()+1)).concat("-").concat(String(today.getDate()));
	document.getElementById("txnDate").value= dateString;
</script>
<script type="text/javascript">
function submitForm()
{
    value = parseFloat(document.getElementById("amount").value);
    newvalue = parseInt(value*100);
    document.getElementById("amount").value=newvalue;
	if(amount()){
	//alert('33');
		return true;
	}
	else {
	alert('You cannot change amount value');
	return false;
	
	}
}
</script>
<script>

function amount(){
	
		$amt= $('#amount').val();
		//alert($amt);
		<?php $pricing = $userForm->Total;
				$i='00';?>
				//alert(<?php echo $pricing.$i ?>);
		if( parseInt(<?php echo $pricing.$i ?>) == $amt) {
		//alert('22');
			return true;
		}
		else{ //alert('11');
			return false;
		}
}

</script>
                
                <?php if($enable_comments_page == 1) : ?>
				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>
                <?php endif; ?>
                
            </div><!-- #primary -->
            
            <?php if($default_page_layout == 'left_sidebar' || $default_page_layout == 'both_sidebar') : ?>
                <?php get_sidebar('left'); ?>
            <?php endif; ?>
            
        <?php if($default_page_layout == 'both_sidebar') : ?>
            </div> <!-- #primary-wrap -->
        <?php endif; ?>
        
        <?php if($default_page_layout == 'right_sidebar' || $default_page_layout == 'both_sidebar') : ?>
            <?php get_sidebar('right'); ?>
        <?php endif; ?>
    </div>
	</main><!-- #main -->
<?php endwhile; // end of the loop. ?>  	
<?php get_footer(); ?>

